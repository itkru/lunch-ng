import { Observable, Subject } from "rxjs";
import { Event } from "../event/event";
import { ObservableProperty, SimpleObservableProperty } from '@wildebeest/observe-changes';

export class Typewriter {
    protected defaultConfig: any = {
        charsPerSecond: 4
    };
    protected config: any = {};
    protected textProperty: ObservableProperty<string>;
    protected dispatcherValue: Subject<Event>;
    private timeout: any = null;

    constructor(config: any = {}) {
        this.textProperty = new SimpleObservableProperty("");
        this.dispatcherValue = new Subject();
        this.config = config;
        for (let key in this.defaultConfig) {
            if (this.config[key]) {
                continue;
            }
            this.config[key] = this.defaultConfig[key];
        }
    }

    public write(text: string, startAt: number = 0): void {
        if (text.length == startAt) {
            this.dispatcherValue.next(new Event("write.finish", null));
            return;
        }
        let time: number = this.generateTime();
        this.timeout = setTimeout(() => {
            this.textProperty.set(text.substr(0, startAt + 1));
            this.write(text, startAt + 1);
        }, time);
    }

    public erase(text: string, endAt: number = 0): void {
        if (text.length == endAt) {
            this.dispatcherValue.next(new Event("erase.finish"));
            return;
        }
        let time: number = this.generateTime();
        this.timeout = setTimeout(() => {
            this.textProperty.set(text.substr(0, text.length - endAt - 1));
            this.erase(text, endAt + 1);
        }, time);
    }

    protected generateTime(): number {
        return 1 / this.config.charsPerSecond * Math.random() * 1000;
    }

    public property(): ObservableProperty<string> {
        return this.textProperty;
    }

    public dispatcher(): Observable<Event> {
        return this.dispatcherValue.asObservable();
    }

    public stop(): void {
        if (this.timeout == null) {
            return;
        }
        clearTimeout(this.timeout);
        this.timeout = null;
    }
}