import { Observable, Subscription } from "rxjs";
import { Typewriter } from "./typewriter.object";
import { AutoEraseTypewriter } from './autoerasetypewriter.object';
import { ObservableProperty } from '@wildebeest/observe-changes';

export class RandomCycleTypewriter {
    private texts: string[];
    private typewriter: AutoEraseTypewriter;
    private textToWrite: string;
    private subscriptions: Subscription[] = [];

    constructor(observableList: Observable<string[]>, config: any = {}) {
        this.typewriter = new AutoEraseTypewriter(
            new Typewriter(config),
            300
        );
        this.texts = [];
        this.subscriptions.push(
            observableList.subscribe((texts: string[]) => {
                this.texts = texts;
                this.start();
            })
        );

        this.subscriptions.push(
            this.typewriter.dispatcher().subscribe((event) => {
                if (event == null) {
                    return;
                }

                if (event.getName() == 'write.finish') {
                    setTimeout(() => {
                        this.start();
                    }, 2000);
                }
            })
        );
    }

    public start(): void {
        if (this.texts.length === 0) {
            return;
        }
        this.textToWrite = this.getRandomText();
        this.typewriter.write(this.textToWrite);
    }

    public stop(): void {
        for (let subscription of this.subscriptions) {
            subscription.unsubscribe();
        }
    }

    private getRandomText(): string {
        return this.texts[Math.min(this.texts.length - 1, Math.floor(Math.random() * this.texts.length))];
    }

    public property(): ObservableProperty<string> {
        return this.typewriter.property();
    }
}