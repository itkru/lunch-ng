import { Typewriter } from './typewriter.object';
import { Event } from '../event/event';
import { Observable } from 'rxjs';
import { ObservableProperty } from '@wildebeest/observe-changes';

export class AutoEraseTypewriter {
    private next: string = "";

    constructor(
        private typewriter: Typewriter,
        private finishDelay: number
    ) {
        this.typewriter.dispatcher()
            .subscribe((event: Event) => {
                if (event == null) {
                    return;
                }

                if (event.getName() == "erase.finish") {
                    setTimeout(() => {
                        this.writeNext();
                    }, finishDelay);
                }
            });
    }

    public write(text: string): void {
        this.next = text;
        if (this.typewriter.property().get().length > 0) {
            this.removeOld();
        } else {
            this.writeNext();
        }
    }

    private removeOld(): void {
        this.typewriter.stop();
        this.typewriter.erase(
            this.typewriter.property().get()
        );
    }

    private writeNext(): void {
        if (this.next.length === 0) {
            return;
        }
        this.typewriter.stop();
        this.typewriter.write(this.next);
        this.next = "";
    }

    public property(): ObservableProperty<string> {
        return this.typewriter.property();
    }

    public dispatcher(): Observable<Event> {
        return this.typewriter.dispatcher();
    }

    public stop(): void {
        this.typewriter.stop();
    }
}