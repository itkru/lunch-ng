export interface Task {
    run(): void;
    stop(): void;
}