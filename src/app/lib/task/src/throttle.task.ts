import { Task } from './task';

export class ThrottleTask implements Task {
    private timeout: any;

    constructor(
        private callback: any,
        private delayMilliseconds: number
    ) {
        this.timeout = null;
    }

    public run(): void {
        if (this.timeout) {
            return;
        }
        this.timeout = setTimeout(() => {
            this.callback();
            this.timeout = null;
        }, this.delayMilliseconds);
    }

    public stop(): void {
        if (this.timeout === null) {
            return;
        }
        clearTimeout(this.timeout);
    }
}