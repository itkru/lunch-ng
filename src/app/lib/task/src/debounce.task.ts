import { Task } from './task';

export class DebounceTask implements Task {
    private timeout: any;

    constructor(
        private callback: any,
        private delayMilliseconds: number
    ) {
        this.timeout = null;
    }

    public run(): void {
        this.stop();
        this.timeout = setTimeout(() => {
            this.callback();
            this.timeout = null;
        }, this.delayMilliseconds);
    }

    public stop(): void {
        if (this.timeout === null) {
            return;
        }
        clearTimeout(this.timeout);
    }
}