import { fakeAsync, tick } from '@angular/core/testing';
import { DebounceTask } from '../src/debounce.task';

describe("DebounceTask", () => {
    it("stop taskWillNotRun", fakeAsync(() => {
        let counter: number = 0;
        let task: DebounceTask = new DebounceTask(
            () => {counter++;},
            100
        );
        task.run();
        tick(50);
        task.stop();
        tick(100);

        expect(counter).toBe(0);
    }));

    it("runTwiceIn50Ms taskRunsOnceAfter100Ms", fakeAsync(() => {
        let counter: number = 0;
        let task: DebounceTask = new DebounceTask(
            () => {counter++;},
            100
        );
        task.run();
        tick(50);
        task.run();
        tick(100);

        expect(counter).toBe(1);
    }));

    it("runTwiceIn200Ms taskRunsTwice", fakeAsync(() => {
        let counter: number = 0;
        let task: DebounceTask = new DebounceTask(
            () => {counter++;},
            100
        );
        task.run();
        tick(100);
        task.run();
        tick(100);

        expect(counter).toBe(2);
    }));

    it("runThreeTimesIn200Ms taskRunsOnce", fakeAsync(() => {
        let counter: number = 0;
        let task: DebounceTask = new DebounceTask(
            () => {counter++;},
            100
        );
        task.run();
        tick(80);
        task.run();
        tick(80);
        task.run();
        tick(100);

        expect(counter).toBe(1);
    }));

    it("runThreeTimesIn200MsStopLast taskWillNotRun", fakeAsync(() => {
        let counter: number = 0;
        let task: DebounceTask = new DebounceTask(
            () => {counter++;},
            100
        );
        task.run();
        tick(80);
        task.run();
        tick(80);
        task.run();
        tick(80);
        task.stop();

        expect(counter).toBe(0);
    }));
});