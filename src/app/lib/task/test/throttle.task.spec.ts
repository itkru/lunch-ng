import { fakeAsync, tick } from '@angular/core/testing';
import { ThrottleTask } from '../src/throttle.task';

describe("ThrottleTask", () => {
    it("stop taskWillNotRun", fakeAsync(() => {
        let counter: number = 0;
        let task: ThrottleTask = new ThrottleTask(
            () => {counter++;},
            100
        );
        task.run();
        tick(50);
        task.stop();
        tick(100);

        expect(counter).toBe(0);
    }));

    it("runTwiceIn100Ms taskRunsOnce", fakeAsync(() => {
        let counter: number = 0;
        let task: ThrottleTask = new ThrottleTask(
            () => {counter++;},
            100
        );
        task.run();
        tick(50);
        task.run();
        tick(50);

        expect(counter).toBe(1);
    }));

    it("runTwiceIn200Ms taskRunsTwice", fakeAsync(() => {
        let counter: number = 0;
        let task: ThrottleTask = new ThrottleTask(
            () => {counter++;},
            100
        );
        task.run();
        tick(100);
        task.run();
        tick(100);

        expect(counter).toBe(2);
    }));

    it("runThreeTimesIn200Ms taskRunsTwice", fakeAsync(() => {
        let counter: number = 0;
        let task: ThrottleTask = new ThrottleTask(
            () => {counter++;},
            100
        );
        task.run();
        tick(80);
        task.run();
        tick(80);
        task.run();
        tick(100);

        expect(counter).toBe(2);
    }));

    it("runThreeTimesIn200MsStopLast taskRunsOnce", fakeAsync(() => {
        let counter: number = 0;
        let task: ThrottleTask = new ThrottleTask(
            () => {counter++;},
            100
        );
        task.run();
        tick(80);
        task.run();
        tick(80);
        task.run();
        tick(80);
        task.stop();
        tick(80);

        expect(counter).toBe(1);
    }));
});