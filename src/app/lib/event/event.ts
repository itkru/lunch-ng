export class Event {
    private name: String;
    private value: any;

    constructor(name: String, value?: any) {
        this.name = name;
        this.value = value;
    }

    public getName(): String {
        return this.name;
    }

    public getValue(): any {
        return this.value;
    }
}