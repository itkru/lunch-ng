import { Event } from "src/app/lib/event/event";

export class SubscriptionAction<T> {
    onActivate(value: T): void {};

    next(event: Event): void {
        this.onActivate(event.getValue());
    }

    error(error: any): void {}

    complete(): void {}
}