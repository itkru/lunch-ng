import { Observable, Subject } from "rxjs";
import { Injectable } from "@angular/core";
import { Event } from "src/app/lib/event/event";

@Injectable()
export class EventDispatcher {
    private map: Map<String, Subject<Event>>;

    constructor() {
        this.map = new Map();
    }

    public dispatch(event: Event) {
        this.getOrCreate(event.getName()).next(event);
    }

    public observe(eventName: String): Observable<Event> {
        return this.getOrCreate(eventName).asObservable();
    }

    private getOrCreate(eventName: String): Subject<Event> {
        if (!this.map.has(eventName)) {
            this.map.set(eventName, new Subject());
        }
        return this.map.get(eventName);
    }
}