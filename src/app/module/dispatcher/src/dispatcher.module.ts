import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventDispatcher } from './eventdispatcher';

@NgModule({
    imports: [CommonModule],
    exports: [],
    declarations: [],
    providers: [EventDispatcher],
})
export class DispatcherModule {
    
}
