import { fakeAsync, TestBed, flush } from "@angular/core/testing";
import { EventDispatcher } from 'src/app/module/dispatcher';
import { Event } from 'src/app/lib/event/event';

describe("EventDispatcher", () => {
    let dispatcher: EventDispatcher;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [EventDispatcher]
        });

        dispatcher = TestBed.get(EventDispatcher);
    });

    it("observe does not fire on initialization", fakeAsync(() => {
        let counter: number = 0;
        dispatcher.observe("test-event")
            .subscribe(() => {
                counter++;
            });
        flush();

        expect(counter).toBe(0);
    }));

    it("observe fires on event dispatch", fakeAsync(() => {
        let counter: number = 0;
        dispatcher.observe("test-event")
            .subscribe(() => {
                counter++;
            });
        flush();

        dispatcher.dispatch(
            new Event("test-event", "value")
        );
        flush();

        expect(counter).toBe(1);
    }));

    it("event contains value on dispatch", fakeAsync(() => {
        let value: string = "";
        dispatcher.observe("test-event")
            .subscribe((event: Event) => {
                value = event.getValue();
            });
        flush();

        dispatcher.dispatch(
            new Event("test-event", "value")
        );
        flush();

        expect(value).toBe("value");
    }));

    it("observe does not fire on different event dispatch", fakeAsync(() => {
        let counter: number = 0;
        dispatcher.observe("test-event")
            .subscribe(() => {
                counter++;
            });
        flush();

        dispatcher.dispatch(
            new Event("wrong-name", "value")
        );
        flush();

        expect(counter).toBe(0);
    }));

    it("observe fires twice for two events", fakeAsync(() => {
        let counter: number = 0;
        dispatcher.observe("test-event")
            .subscribe(() => {
                counter++;
            });
        flush();

        dispatcher.dispatch(
            new Event("test-event", "value")
        );
        flush();

        dispatcher.dispatch(
            new Event("test-event", "value")
        );
        flush();

        expect(counter).toBe(2);
    }));

    it("observe fires twice for the same event", fakeAsync(() => {
        let counter: number = 0;
        dispatcher.observe("test-event")
            .subscribe(() => {
                counter++;
            });
        flush();

        let event: Event = new Event("test-event", "value");
        dispatcher.dispatch(event);
        flush();
        
        dispatcher.dispatch(event);
        flush();

        expect(counter).toBe(2);
    }));
});