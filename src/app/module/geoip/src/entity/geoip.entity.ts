export class GeoipEntity {
    constructor(
        private longitude: string,
        private latitude: string,
        private city: string = ""
    ) {}

    public getLongitude(): string {
        return this.longitude;
    }

    public getLatitude(): string {
        return this.latitude;
    }

    public getCity(): string {
        return this.city;
    }

    public display(): string {
        if (this.city.length > 0) {
            return this.getCity();
        }
        return this.getLatitude() + ", " + this.getLongitude();
    }
}