import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestSender, RequestFactory, RequestEntity } from 'src/app/module/request';

@Injectable()
export class GeoipApi {
    constructor(
        private requestSender: RequestSender,
        private requestFactory: RequestFactory
    ) {}

    public identify(ip: string): Observable<any> {
        let request: RequestEntity = this.requestFactory.createBuilder()
            .withMethod('GET')
            .withUrl('https://ipgeolocation.com/' + ip)
            .withQuery('json', '1')
            .withoutCredentials()
            .build();

        return this.requestSender.send(request);
    }

    public findIp(): Observable<any> {
        let request: RequestEntity = this.requestFactory.createBuilder()
            .withMethod('GET')
            .withUrl('https://jsonip.com')
            .withoutCredentials()
            .build();

        return this.requestSender.send(request);
    }
}