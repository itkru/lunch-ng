import { GeoipEntity } from '../entity/geoip.entity';
import { Injectable } from '@angular/core';
import { ObservableProperty, SimpleObservableProperty } from '@wildebeest/observe-changes';

@Injectable()
export class GeoipStore {
    private userGeoip: ObservableProperty<GeoipEntity>;
    private userIp: ObservableProperty<string>;

    constructor() {
        this.userGeoip = new SimpleObservableProperty();
        this.userIp = new SimpleObservableProperty();
    }

    public getUserGeoip(): ObservableProperty<GeoipEntity> {
        return this.userGeoip;
    }

    public getUserIp(): ObservableProperty<string> {
        return this.userIp;
    }
}