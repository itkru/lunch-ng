import { NgModule } from '@angular/core';
import { GeoipStore } from './service/geoip.store';
import { GeoipApi } from './service/geoip.api';

@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [GeoipStore, GeoipApi]
})
export class GeoipModule {

}