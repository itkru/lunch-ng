import { Injectable } from "@angular/core";
import { ObservableList, SimpleObservableList } from '@wildebeest/observe-changes';

@Injectable()
export class GreetingsStore {
    private list: ObservableList<string>;

    constructor() {
        this.list = new SimpleObservableList([]);
    }

    public getList(): ObservableList<string> {
        return this.list;
    }

    public add(greeting: string): void {
        this.list.add(greeting);
    }

    public addAll(greetings: string[]): void {
        this.list.addAll(greetings);
    }
}