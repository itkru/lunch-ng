import { NgModule } from "@angular/core";
import { GreetingsStore } from "./greetings.store";

@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [GreetingsStore]
})
export class GreetingsModule {
    constructor(
        private greetingsStore: GreetingsStore
    ) {
        greetingsStore.addAll([]);
    }
}