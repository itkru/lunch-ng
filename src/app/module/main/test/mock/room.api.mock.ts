import { RoomApi } from 'src/app/module/room/src/service/room.api';
import { Observable } from 'rxjs';

export class RoomApiMock extends RoomApi {
    constructor(
        private createResult: any,
        private loadResult: any
    ) {
        super(null, null);
    }

    public setCreate(createResult: any): void {
        this.createResult = createResult;
    }

    public setLoad(loadResult: any): void {
        this.loadResult = loadResult;
    }

    create(): Observable<any> {
        return new Observable((subscriber) => {
            subscriber.next(this.createResult);
            subscriber.complete();
        });
    }

    load(): Observable<any> {
        return new Observable((subscriber) => {
            subscriber.next(this.loadResult);
            subscriber.complete();
        });
    }
}