import { fakeAsync, TestBed, discardPeriodicTasks, flushMicrotasks, flush } from "@angular/core/testing";
import { RoomService } from '../../src/room/room.service';
import { EventDispatcher } from 'src/app/module/dispatcher';
import { Event } from 'src/app/lib/event/event';
import { RoomModule, RoomStore } from 'src/app/module/room';
import { RequestModule } from 'src/app/module/request';
import { PlaceModule } from 'src/app/module/place';
import { LoggerModule } from 'src/app/module/logger';
import { GeoipModule } from 'src/app/module/geoip';
import { RoomApi } from 'src/app/module/room/src/service/room.api';
import { RoomApiMock } from '../mock/room.api.mock';
import { ComponentModelRepository } from 'src/app/module/gui/src/service/component-model.repository';
import { AppStore } from '../../src/app.store';

describe("RoomService", () => {
    let roomService: RoomService;
    let dispatcher: EventDispatcher;
    let roomApi: RoomApiMock;
    let roomStore: RoomStore;

    beforeEach(() => {
        roomApi = new RoomApiMock(null, null);
        TestBed.configureTestingModule({
            imports: [RequestModule, RoomModule, PlaceModule, LoggerModule, GeoipModule],
            providers: [
                RoomService,
                EventDispatcher,
                RoomStore,
                ComponentModelRepository,
                AppStore,
                {
                    provide: RoomApi,
                    useValue: roomApi
                }
            ]
        });

        dispatcher = TestBed.get(EventDispatcher);
        roomService = TestBed.get(RoomService);
        roomStore = TestBed.get(RoomStore);
    });

    it("start listens for openRoom", fakeAsync(() => {
        roomApi.setCreate({
            room: {
                id: "1",
                name: "test"
            }
        });
       roomService.start(); 
       discardPeriodicTasks();

       dispatcher.dispatch(
           new Event("room.open", "test")
       );
       flush();

        expect(
            roomStore.active().get().getId()
        ).toBe("1");
    }));

    it("start listens for closeRoom", fakeAsync(() => {
        roomApi.setCreate({
            room: {
                id: "1",
                name: "test"
            }
        });
        roomService.start(); 
        discardPeriodicTasks();
       
        dispatcher.dispatch(
            new Event("room.open", "test")
        );
        flush();

       dispatcher.dispatch(
           new Event("room.close")
       );
       flush();

        expect(
            roomStore.active().get()
        ).toBeNull();
    }));

    it("start listens for refreshRoom", fakeAsync(() => {
        roomApi.setCreate({
            room: {
                id: "1",
                name: "test",
                activities: []
            }
        });

        roomApi.setLoad({
            room: {
                id: "1",
                name: "test2",
                activities: []
            }
        });
        roomService.start(); 
        discardPeriodicTasks();

        dispatcher.dispatch(
            new Event("room.open", "test")
        );
        flush();
       
        dispatcher.dispatch(
            new Event("room.refresh")
        );
        flush();

        expect(
            roomStore.active().get().getName()
        ).toBe("test2");
    }));

    it("start listens for refreshRoom does nothing when room not open", fakeAsync(() => {
        roomApi.setLoad({
            room: {
                id: "1",
                name: "test2",
                activities: []
            }
        });
        roomService.start(); 
        discardPeriodicTasks();
       
        dispatcher.dispatch(
            new Event("room.refresh")
        );
        flush();

        expect(
            roomStore.active().get()
        ).toBeNull();
    }));

    it("start listens for refreshSilentRoom", fakeAsync(() => {
        roomApi.setCreate({
            room: {
                id: "1",
                name: "test",
                activities: []
            }
        });

        roomApi.setLoad({
            room: {
                id: "1",
                name: "test2",
                activities: []
            }
        });
        roomService.start(); 
        discardPeriodicTasks();

        dispatcher.dispatch(
            new Event("room.open", "test")
        );
        flush();
       
        dispatcher.dispatch(
            new Event("room.refresh.silent")
        );
        flush();

        expect(
            roomStore.active().get().getName()
        ).toBe("test2");
    }));

    it("start listens for refreshSilentRoom does nothing when room not open", fakeAsync(() => {
        roomApi.setLoad({
            room: {
                id: "1",
                name: "test2",
                activities: []
            }
        });
        roomService.start(); 
        discardPeriodicTasks();
       
        dispatcher.dispatch(
            new Event("room.refresh.silent")
        );
        flush();

        expect(
            roomStore.active().get()
        ).toBeNull();
    }));
});