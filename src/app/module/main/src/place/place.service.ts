import { Injectable } from "@angular/core";
import { EventDispatcher } from "src/app/module/dispatcher";
import { PlaceEntity, PlaceStore, PlaceFactory } from "src/app/module/place";
import { Event } from "src/app/lib/event/event";
import { ActivityStore } from "src/app/module/activity";
import { MapToMapLink, Closable, MapChange, ObservableProperty } from '@wildebeest/observe-changes';
import { ActivityToPlaceModifier } from './activity-to-place.modifier';

@Injectable()
export class PlaceService {
    private closables: Closable[] = [];

    constructor(
        private dispatcher: EventDispatcher,
        private placeStore: PlaceStore,
        private placeFactory: PlaceFactory,
        private activityStore: ActivityStore
    ) {}

    public start(): void {
        this.closables.push(
            new MapToMapLink(
                this.activityStore.activities(),
                this.placeStore.places(),
                [new ActivityToPlaceModifier()]
            )
        );

        this.closables.push(
            this.placeStore.places().addListenerAndCall(
                this.onPlacesChange.bind(this)
            )
        );

        this.dispatcher.observe('place.open')
            .subscribe((event: Event) => {
                this.open(event.getValue());
            });

        this.dispatcher.observe('place.close')
            .subscribe((event: Event) => {
                this.placeStore.dectivate();
            });
    }

    private onPlacesChange(change: MapChange<string, PlaceEntity>): void {
        let placeProperty: ObservableProperty<PlaceEntity> = this.placeStore.active();
        if (placeProperty.isEmpty() || placeProperty.get().getExternalId() !== "__pending") {
            return;
        }

        let toActivate: PlaceEntity = this.placeStore.places().get(placeProperty.get().getId());
        if (toActivate === null) {
            return;
        }
        this.placeStore.activate(toActivate);
    }

    private open(placeId: string): void {
        let toActivate: PlaceEntity = this.placeStore.places().get(placeId);
        if (toActivate === null) {
            toActivate = this.placeFactory.create(placeId, "", "__pending", null);
        }
        
        this.placeStore.activate(toActivate);
    }
}