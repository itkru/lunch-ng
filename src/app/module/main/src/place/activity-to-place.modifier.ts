import { Modifier, MapChange, MapEntry } from '@wildebeest/observe-changes';
import { ActivityEntity } from 'src/app/module/activity';
import { PlaceEntity } from 'src/app/module/place';

export class ActivityToPlaceModifier implements Modifier<MapChange<string, ActivityEntity>, MapChange<string, PlaceEntity>> {
    public modify(change: MapChange<string, ActivityEntity>): MapChange<string, PlaceEntity> {
        let toInsert: MapEntry<string, PlaceEntity>[] = [];
        for (let inserted of change.inserted()) {
            let place: PlaceEntity = inserted.getValue().getPlace();
            toInsert.push(
                new MapEntry(place.getId(), place)
            );
        }

        let toRemove: MapEntry<string, PlaceEntity>[] = [];
        for (let removed of change.removed()) {
            let place: PlaceEntity = removed.getValue().getPlace();
            toRemove.push(
                new MapEntry(place.getId(), place)
            );
        }

        return new MapChange(toInsert, toRemove);
    }
}