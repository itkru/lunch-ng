import { GeoipApi, GeoipStore, GeoipEntity } from 'src/app/module/geoip';
import { Injectable } from '@angular/core';
import { EventDispatcher } from 'src/app/module/dispatcher';
import { Event } from 'src/app/lib/event/event';
import { TranslateService } from '@ngx-translate/core';
import { PropertyChange } from '@wildebeest/observe-changes';

@Injectable()
export class GeolocationService {
    constructor(
        private geoipApi: GeoipApi,
        private geoipStore: GeoipStore,
        private dispatche: EventDispatcher,
        private translateService: TranslateService
    ) {}

    public start(): void {
        this.geoipStore.getUserIp().addListenerAndCall(
            this.onIpChange.bind(this)
        );

        this.geoipApi.findIp()
            .subscribe({
                next: (result: any) => {
                    this.geoipStore.getUserIp().set(result.ip);
                },
                error: (err: any) => {
                    console.log("ip err", err);
                }
            });

        this.dispatche.observe("gps.request")
            .subscribe((event: Event) => {
                if (!navigator.geolocation) {
                    this.dispatche.dispatch(
                        new Event("toast.show", this.translateService.instant('geo.not-supported-error'))
                    );
                    return;
                } else {
                    navigator.geolocation.getCurrentPosition(
                        (geolocation: any) => {
                            this.geoipStore.getUserGeoip()
                                .set(
                                    new GeoipEntity(geolocation.coords.longitude, geolocation.coords.latitude)
                                );
                        },
                        (err: any) => {
                            this.dispatche.dispatch(
                                new Event("toast.show", this.translateService.instant('geo.current-error'))
                            );
                        }
                    );
                }
            });
    }

    private onIpChange(change: PropertyChange<string>): void {
        let ip: string = change.next();
        if (!ip) {
            return;
        }
        this.geoipApi.identify(ip)
            .subscribe({
                next: (response: any) => {
                    let coords: string[] = response.coords.split(",");
                    this.geoipStore.getUserGeoip().set(
                        new GeoipEntity(coords[1], coords[0], response.city)
                    );
                },
                error: (err: any) => {
                    console.log("identify err", err);
                }
            });
    }
}