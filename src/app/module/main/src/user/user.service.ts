import { Injectable } from "@angular/core";
import { EventDispatcher } from "src/app/module/dispatcher";
import { Event } from "src/app/lib/event/event";
import { GuestApi, GuestStore, GuestEntity } from "src/app/module/guest";
import { ProxyLogger } from "src/app/module/logger";
import { TranslateService } from '@ngx-translate/core';
import { ComponentModelRepository } from 'src/app/module/gui/src/service/component-model.repository';
import { Closable } from '@wildebeest/observe-changes';

@Injectable()
export class UserService {
    constructor(
        private dispatcher: EventDispatcher,
        private guestApi: GuestApi,
        private guestStore: GuestStore,
        private proxyLogger: ProxyLogger,
        private translateService: TranslateService,
        private modelRepository: ComponentModelRepository
    ) {}

    public start(): void {
        this.dispatcher.observe("user.update")
            .subscribe((event: Event) => {
                if (event == null) {
                    return;
                }
                this.updateUser(this.guestStore.authUser().get(), event.getValue())
            });
    }

    private updateUser(user: GuestEntity, data: any): void {
        let loading: Closable = this.modelRepository.get("step:profile").loading().start();
        this.guestApi.save({
                name: data.name
            })
            .subscribe({
                next: (result: any) => {
                    this.dispatcher.dispatch(
                        new Event(
                            "toast.show",
                            this.translateService.instant('profile.profile.save-toast')
                        )
                    );
                },
             error: (err: any) => {
                loading.close();
                this.proxyLogger.alert(err.message, {exception: err});
                this.dispatcher.dispatch(
                    new Event(
                        "toast.show",
                        this.translateService.instant('profile.profile.save-error-toast')
                    )
                );
            },
            complete: () => {
                loading.close();
            }
        });
    }
}