import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { GuestStore } from "src/app/module/guest";
import { Injectable } from "@angular/core";

@Injectable()
export class ProfileResolver implements Resolve<any> {
    constructor(
        private guestStore: GuestStore
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        return {
            user: this.guestStore.authUser()
        };
    }
}