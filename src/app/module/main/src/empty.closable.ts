import { Closable } from '@wildebeest/observe-changes';

export class EmptyClosable implements Closable {
    public close(): void {}
}