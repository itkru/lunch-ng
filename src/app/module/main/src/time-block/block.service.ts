import { Injectable } from '@angular/core';
import { MapChange, ObservableList, MapToGroupLink, ObservableGroup, SimpleObservableGroup, SimpleModifier } from '@wildebeest/observe-changes';
import { ActivityEntity, ActivityStore } from 'src/app/module/activity';
import { TimeBlockStore } from './time-block.store';
import { format } from 'date-fns';
import { TimeBlockService } from './time-block.service';
import { RecommendationStore } from '../recommendation/recommendation.store';
import { PlaceEntity } from 'src/app/module/place';

@Injectable()
export class BlockService {
    private timeBlockServices: Map<string, TimeBlockService>;

    public constructor(
        private activityStore: ActivityStore,
        private timeBlockStore: TimeBlockStore,
        private recommenationStore: RecommendationStore
    ) {
        this.timeBlockServices = new Map();
    }

    public start(): void {
        let activitiesGroup: ObservableGroup<string, ActivityEntity> = new SimpleObservableGroup();

        new MapToGroupLink(
            this.activityStore.activities(),
            activitiesGroup,
            new SimpleModifier((item: ActivityEntity) => {
                return format(item.getStartAt(), "HH:mm");
            })
        );

        activitiesGroup.addListenerAndCall(
            this.onActivityGroupChange.bind(this)
        );

        this.recommenationStore.recommendations().addListenerAndCall(
            this.onRecommendationChange.bind(this)
        );
    }

    private onActivityGroupChange(change: MapChange<string, ObservableList<ActivityEntity>>): void {
        for (let removed of change.removed()) {
            if (!this.timeBlockServices.has(removed.getKey())) {
                continue;
            }
            let service: TimeBlockService = this.timeBlockServices.get(removed.getKey());
            
            service.unbindActivities();
            if (!service.isDead()) {
                continue;
            }
            service.stop();
            this.timeBlockServices.delete(removed.getKey());
        }

        for (let inserted of change.inserted()) {
            if (!this.timeBlockServices.has(inserted.getKey())) {
                let service: TimeBlockService = this.createServiceFromHours(inserted.getKey());
                this.timeBlockServices.set(inserted.getKey(), service);
            }
            let service: TimeBlockService = this.timeBlockServices.get(inserted.getKey());
            service.bindActivities(inserted.getValue());
        }
    }

    private onRecommendationChange(change: MapChange<string, ObservableList<PlaceEntity>>): void {
        for (let removed of change.removed()) {
            if (!this.timeBlockServices.has(removed.getKey())) {
                continue;
            }
            let service: TimeBlockService = this.timeBlockServices.get(removed.getKey());
            service.unbindRecommendations();
            if (!service.isDead()) {
                continue;
            }
            service.stop();
            this.timeBlockServices.delete(removed.getKey());
        }

        for (let inserted of change.inserted()) {
            if (!this.timeBlockServices.has(inserted.getKey())) {
                let service: TimeBlockService = this.createServiceFromHours(inserted.getKey());
                this.timeBlockServices.set(inserted.getKey(), service);
            }
            let service: TimeBlockService = this.timeBlockServices.get(inserted.getKey());
            service.bindRecommendations(inserted.getValue());
        }
    }

    private createServiceFromHours(hours: string): TimeBlockService {
        let hoursSplit: string[] = hours.split(":");
        let time: Date = new Date();
        time.setHours(parseInt(hoursSplit[0]), parseInt(hoursSplit[1]), 0, 0);
        return new TimeBlockService(
            this.timeBlockStore,
            time
        );
    }
}