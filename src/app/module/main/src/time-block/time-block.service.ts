import { ObservableList, Closable, SimpleObservableList, ListChange } from '@wildebeest/observe-changes';
import { ActivityEntity } from 'src/app/module/activity';
import { TimeBlockEntity } from 'src/app/module/gui';
import { TimeBlockStore } from './time-block.store';
import { PlaceEntity } from 'src/app/module/place';

export class TimeBlockService {
    private activitiesCloasbles: Closable[] = [];
    private recommendationsCloasbles: Closable[] = [];
    private timeBlock: TimeBlockEntity

    public constructor(
        private timeBlockStore: TimeBlockStore,
        time: Date
    ) {
        this.timeBlock = new TimeBlockEntity(
            time,
            new SimpleObservableList(),
            new SimpleObservableList()
        );
        this.timeBlockStore.blocks().add(this.timeBlock);
    }

    public stop(): void {
        this.timeBlockStore.blocks().remove(this.timeBlock);
        this.unbindActivities();
        this.unbindRecommendations();
    }

    public isDead(): boolean {
        return this.timeBlock.activities().isEmpty() && this.timeBlock.recommendations().isEmpty();
    }

    public unbindActivities(): void {
        for (let closable of this.activitiesCloasbles) {
            closable.close();
        }
        this.activitiesCloasbles = [];
    }

    public bindActivities(activities: ObservableList<ActivityEntity>): void {
        this.unbindActivities();

        this.activitiesCloasbles.push(
            activities.addListenerAndCall(
                this.onActivitiesChange.bind(this)
            )
        );
    }

    public unbindRecommendations(): void {
        for (let closable of this.recommendationsCloasbles) {
            closable.close();
        }
        this.recommendationsCloasbles = [];
    }

    public bindRecommendations(recommendations: ObservableList<PlaceEntity>): void {
        this.unbindRecommendations();

        this.recommendationsCloasbles.push(
            recommendations.addListenerAndCall(
                this.onRecommendationsChange.bind(this)
            )
        );
    }

    private onActivitiesChange(change: ListChange<ActivityEntity>): void {
        this.timeBlock.activities().removeAll(change.removed());

        this.timeBlock.activities().addAll(change.inserted());
        for (let inserted of change.inserted()) {
            for (let place of this.timeBlock.recommendations().all()) {
                if (place.getId() !== inserted.place.getId()) {
                    continue;
                }
                this.timeBlock.recommendations().remove(place);
            }
        }
    }

    private onRecommendationsChange(change: ListChange<PlaceEntity>): void {
        this.timeBlock.recommendations().removeAll(change.removed());

        for (let inserted of change.inserted()) {
            if (this.hasActivityWithPlace(inserted)) {
                continue;
            }
            this.timeBlock.recommendations().add(inserted);
        }
    }

    private hasActivityWithPlace(place: PlaceEntity): boolean {
        for (let activity of this.timeBlock.activities().all()) {
            if (activity.getPlace().getId() !== place.getId()) {
                continue;
            }
            return true;
        }
        return false;
    }
}