import { Injectable } from '@angular/core';
import { ObservableList, SimpleObservableList } from '@wildebeest/observe-changes';
import { TimeBlockEntity } from 'src/app/module/gui';

@Injectable()
export class TimeBlockStore {
    private blocksValue: ObservableList<TimeBlockEntity>;

    public constructor() {
        this.blocksValue = new SimpleObservableList();

        this.blocksValue.addListener(() => {
            this.blocksValue.all().sort((a, b) => {
                return a.getTime().getTime() - b.getTime().getTime();
            })
        });
    }

    public blocks(): ObservableList<TimeBlockEntity> {
        return this.blocksValue;
    }
}