import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { AppStore } from '../app.store';

@Injectable()
export class LanguageResolver implements Resolve<any> {
    constructor(
        private appStore: AppStore
    ) {}

    resolve(): any {
        return {
            value: this.appStore.language()
        };
    }
}