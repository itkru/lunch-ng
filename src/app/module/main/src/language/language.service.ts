declare var Cookies: any;

import { EventDispatcher } from 'src/app/module/dispatcher';
import { Event } from 'src/app/lib/event/event';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { AppStore } from '../app.store';
import { PropertyChange } from '@wildebeest/observe-changes';

@Injectable()
export class LanguageService {
    constructor(
        private appStore: AppStore,
        private dispatcher: EventDispatcher,
        private translateService: TranslateService
    ) {}

    public start(): void {
        let languages: string[] = ['SK', 'EN'];
        this.translateService.addLangs(languages);
        this.translateService.setDefaultLang('en');

        let initLanguage: string = "";
        if (Cookies.get('language')) {
            initLanguage = Cookies.get('language');
            Cookies.set('language', initLanguage, {expires: 365});
        } else {
            initLanguage = this.translateService.getBrowserLang();
        }

        if (languages.indexOf(initLanguage.toUpperCase()) > -1) {
            this.appStore.language().set(initLanguage);
        }

        this.appStore.language().addListenerAndCall(
            this.onLanguageChange.bind(this)
        );

        this.dispatcher.observe("language.set")
            .subscribe((event: Event) => {
                Cookies.set('language', event.getValue(), {expires: 365});
                this.onLanguageSet(event.getValue());
            });
    }

    private onLanguageSet(language: string): void {
        this.appStore.language()
            .set(language);
    }

    private onLanguageChange(change: PropertyChange<string>): void {
        this.translateService.use(change.next());
    }
}