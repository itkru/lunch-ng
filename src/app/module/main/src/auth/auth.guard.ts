import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { GuestStore, GuestEntity, GuestApi } from "src/app/module/guest";
import { map } from "rxjs/operators";
import { ObservableProperty } from '@wildebeest/observe-changes';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private guestStore: GuestStore,
        private guestApi: GuestApi
    ) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let authUserProperty: ObservableProperty<GuestEntity> = this.guestStore.authUser();
        if (authUserProperty.get()) {
            return true;
        }
        return this.guestApi.autoLogin()
            .pipe(
                map((e) => {
                    if (authUserProperty.get()) {
                        return true;
                    }
        
                    let roomId: string = next.paramMap.get('roomId');
                    this.router.navigate([roomId, 'welcome'], {
                        queryParams: {
                            redirectTo: state.url
                        }
                    });
        
                    return false;
                })
            );
    }
}