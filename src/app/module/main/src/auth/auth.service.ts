import { Injectable } from "@angular/core";
import { EventDispatcher } from "src/app/module/dispatcher";
import { GuestApi, GuestStore, GuestFactory } from "src/app/module/guest";
import { ActivatedRoute, Router, ParamMap } from "@angular/router";
import { ProxyLogger } from "src/app/module/logger";
import { ComponentModelRepository } from 'src/app/module/gui/src/service/component-model.repository';
import { Closable } from '@wildebeest/observe-changes';

@Injectable()
export class AuthService {
    private routeQueryParams: ParamMap;

    constructor(
        private dispatcher: EventDispatcher,
        private guestApi: GuestApi,
        private guestStore: GuestStore,
        private gusetFactory: GuestFactory,
        private route: ActivatedRoute,
        private router: Router,
        private modelRepository: ComponentModelRepository,
        private proxyLogger: ProxyLogger
    ) {}

    public start(): void {
        this.route.queryParamMap.subscribe(paramMap => {
            this.routeQueryParams = paramMap;
        });

        this.dispatcher.observe("auth.signup")
            .subscribe(event => {
                this.onSignup(event.getValue());
            });
    }

    private onSignup(name: string): void {
        let loading: Closable = this.modelRepository.get("auth").loading().start();
        this.guestApi.save({name: name})
            .subscribe({
                next: (result: any) => {
                    let user: any = result.user;
                    this.guestStore.authenticateUser(
                        this.gusetFactory.createByObject(user)
                    );
                    let redirectTo: string = this.routeQueryParams.get('redirectTo');
                    if (redirectTo) {
                        this.router.navigateByUrl(redirectTo);
                    }
                },
                error: (error: any) => {
                    loading.close();
                    this.proxyLogger.emergency(error.message, {exception: error});
                },
                complete: () => {
                    loading.close();
                }
            });
    }
}