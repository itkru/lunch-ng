import { Resolve } from "@angular/router";
import { Injectable } from "@angular/core";
import { AppStore } from '../app.store';

@Injectable()
export class AuthResolver implements Resolve<any> {
    constructor(
        private appStore: AppStore
    ) {}

    resolve(): any {
        return {
            user: this.appStore.user()
        };
    }
}