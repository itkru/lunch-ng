import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { tap, catchError } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { GuestStore, GuestFactory } from "src/app/module/guest";

@Injectable()
export class AuthSetInterceptor implements HttpInterceptor {
    constructor (
        private guestStore: GuestStore,
        private guestFactory: GuestFactory
    ) {}

    private response(event: HttpResponse<any>): any {
        if (event.body && event.body.user) {
            this.guestStore.authenticateUser(
                this.guestFactory.createByObject(event.body.user)
            );
        }
    }

    public intercept (request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(this.response.bind(this)),
            catchError((error, obs: Observable<any>) => {
                if (error.status == 400) {
                    this.guestStore.signOut();
                }
                return throwError(error);
            })
        );
    }
}