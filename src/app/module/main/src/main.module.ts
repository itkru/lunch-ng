import { NgModule, ErrorHandler } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { DispatcherModule } from "../../dispatcher";

import { ActivityService } from "./activity/activity.service";
import { RoomService } from "./room/room.service";
import { AppView } from "../../gui";
import { ToastModule, ToastService } from "../../toast";
import { GuiModule, HomeView, RoomView, RoomShellView, StepInfoView, StepPlaceView, StepTimeView, ProfileView } from "../../gui";
import { GreetingsModule } from "../../greetings";
import { RoomResolver } from "./room/room.resolver";
import { RoomModule } from "../../room";
import { WelcomeView } from "../../gui/src/view/welcome/welcome.view";
import { AuthGuard } from "./auth/auth.guard";
import { AuthService } from "./auth/auth.service";
import { GuestModule } from "../../guest";
import { RequestModule, RequestFactory } from "../../request";
import { environment } from "src/environments/environment";
import { HTTP_INTERCEPTORS, HttpClient } from "@angular/common/http";
import { AuthSetInterceptor } from "./auth/auth-set.interceptor";
import { PlaceModule } from "../../place";
import { ActivityModule } from "../../activity";
import { StepResolver } from "./room/step.resolver";
import { ProfileResolver } from "./user/profile.resolver";
import { UserService } from "./user/user.service";
import { RoomViewResolver } from "./room/room-view.resolver";
import { StepTimeGuard } from "./room/step-time.guard";
import { StepPlaceGuard } from "./room/step-place.guard";
import { ProxyLogger, LoggerModule } from "../../logger";
import { AuthResolver } from "./auth/auth.resolver";
import { PlaceService } from "./place/place.service";
import { NotificationService } from "./notification/notification.service";
import { BrowserNotificationModule } from "../../browser-notification";
import { SentryLogger } from "../../sentry";
import { AnalyticsModule, PageviewProxy } from "../../analytics";
import { AnalyticsService } from "./analytics/analytics.service";
import { GtmApi } from "../../gtm";
import { LanguageService } from './language/language.service';
import { LanguageResolver } from './language/language.resolver';
import { GreetingsService } from './greetings/greetings.service';
import { GeoipModule } from '../../geoip';
import { GeolocationService } from './geolocation/geolocation.service';
import { TimeBlockStore } from './time-block/time-block.store';
import { BlockService } from './time-block/block.service';
import { RecommendationService } from './recommendation/recommendation.service';
import { RecommendationStore } from './recommendation/recommendation.store';
import { AppStore } from './app.store';

let routes: Routes = [
    {
        path: ':roomId/welcome',
        component: WelcomeView,
        resolve: {
            auth: AuthResolver
        }
    },
    {
        path: ':roomId',
        component: RoomShellView,
        canActivate: [AuthGuard],
        resolve: {
            room: RoomResolver
        },
        children: [
            {
                path: 'place/:placeId',
                component: RoomView,
                resolve: {
                    view: RoomViewResolver
                }
            },
            {
                path: 'profile',
                component: ProfileView,
                resolve: {
                    profile: ProfileResolver
                }
            },
            {
                path: 'create/time',
                component: StepTimeView,
                resolve: {
                    step: StepResolver
                }
            },
            {
                path: 'create/place',
                component: StepPlaceView,
                resolve: {
                    step: StepResolver
                },
                canActivate: [StepTimeGuard],
            },
            {
                path: 'create/info',
                component: StepInfoView,
                resolve: {
                    step: StepResolver
                },
                canActivate: [StepPlaceGuard],
            },
            {
                path: '',
                component: RoomView,
                resolve: {
                    view: RoomViewResolver
                }
            }
        ]
    },
    {
        path: '',
        component: HomeView,
        resolve: {
            language: LanguageResolver
        }
    }
];

export function HttpLoaderFactory(httpClient: HttpClient) {
    return new TranslateHttpLoader(httpClient, environment.host + '/assets/i18n/', '.json');
}

@NgModule({
    imports: [
        BrowserModule, RouterModule.forRoot(routes), CommonModule, FormsModule,
        DispatcherModule, RequestModule, LoggerModule,
        GuestModule, RoomModule, PlaceModule, ActivityModule,
        GreetingsModule, ToastModule, GuiModule, BrowserNotificationModule, AnalyticsModule, TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }), GeoipModule
    ],
    exports: [],
    declarations: [],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthSetInterceptor,
            multi: true
        },
        {provide: ErrorHandler, useExisting: ProxyLogger},
        TimeBlockStore, RecommendationStore, AuthService, RoomService, ToastService, ActivityService, UserService, PlaceService, NotificationService, AnalyticsService, LanguageService, GreetingsService, GeolocationService, BlockService, RecommendationService, AppStore,
        RoomResolver, AuthGuard, StepResolver, ProfileResolver, RoomViewResolver, StepTimeGuard, StepPlaceGuard, AuthResolver, LanguageResolver
    ],
    bootstrap: [AppView]
})
export class MainModule {
    constructor(
        activityService: ActivityService,
        roomService: RoomService,
        toastService: ToastService,
        authService: AuthService,
        requestFactory: RequestFactory,
        userServie: UserService,
        placeService: PlaceService,
        notificationService: NotificationService,
        proxyLogger: ProxyLogger,
        pageviewProxy: PageviewProxy,
        analyticsService: AnalyticsService,
        languageService: LanguageService,
        greetingsService: GreetingsService,
        geolocationService: GeolocationService,
        blockService: BlockService,
        recommendationService: RecommendationService
    ) {
        requestFactory.setBase(environment.server.host);
        if (environment.log.use == 'sentry') {
            proxyLogger.setProxy(
                new SentryLogger(environment.log.sentry)
            );
        }
      
        if (environment.analytics.use == 'gtag') {
            pageviewProxy.setProxy(
                new GtmApi(environment.analytics.gtag)
            );
        }

        geolocationService.start();
        languageService.start();
        greetingsService.start();
        authService.start();
        toastService.start();
        roomService.start();
        activityService.start();
        userServie.start();
        placeService.start();
        notificationService.start();
        analyticsService.start();
        blockService.start();
        recommendationService.start();
    }
}