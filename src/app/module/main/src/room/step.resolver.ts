import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { GeoipStore } from 'src/app/module/geoip';
import { RoomStore } from 'src/app/module/room';
import { AppStore } from '../app.store';

@Injectable()
export class StepResolver implements Resolve<any> {
    constructor(
        private appStore: AppStore,
        private roomStore: RoomStore,
        private geoipStore: GeoipStore
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        return {
            activityForm: this.appStore.stepActivity(),
            placeAutocomplete: this.appStore.placeAutocomplete(),
            room: this.roomStore.active(),
            geoip: this.geoipStore.getUserGeoip()
        };
    }
}