import { Injectable } from "@angular/core";
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from "@angular/router";
import { AppStore } from '../app.store';

@Injectable()
export class StepTimeGuard implements CanActivate {
    constructor(
        private appStore: AppStore,
        private router: Router
    ) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let activity: any = this.appStore.stepActivity().get();
        if (activity && activity.startAt) {
            return true;
        }

        this.router.navigate([
            next.parent.paramMap.get('roomId'),
            'create',
            'time'
        ]);

        return false;
    }
}