import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { AppStore } from '../app.store';

@Injectable()
export class StepPlaceGuard implements CanActivate {
    constructor(
        private appStore: AppStore,
        private router: Router
    ) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let activity: any = this.appStore.stepActivity().get();
        if (activity !== null && activity !== undefined) {
            let place: any = activity.place;

            if (place.name !== "" || place.id !== "") {
                return true;
            }
        }
        
        this.router.navigate([
            next.parent.paramMap.get('roomId'),
            'create',
            'place'
        ]);
        return false;
    }
}