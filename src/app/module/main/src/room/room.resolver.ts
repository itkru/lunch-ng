import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { EventDispatcher } from "src/app/module/dispatcher";
import { Event } from "src/app/lib/event/event";
import { RoomEntity, RoomStore } from "src/app/module/room";

@Injectable()
export class RoomResolver implements Resolve<any> {
    constructor(
        private dispatcher: EventDispatcher,
        private roomStore: RoomStore
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        let room: RoomEntity = this.roomStore.active().get();
        let roomId: string = route.paramMap.get('roomId');
        if (!room || room.getName() != roomId) {
            this.dispatcher.dispatch(
                new Event("room.open", roomId)
            );
        }
        return this.roomStore.active();
    }
}