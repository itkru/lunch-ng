import { SubscriptionAction, EventDispatcher } from "src/app/module/dispatcher";
import { RoomApi } from "src/app/module/room/src/service/room.api";
import { RoomStore, RoomFactory, RoomEntity } from "src/app/module/room";
import { ProxyLogger } from "src/app/module/logger";
import { AfterRoomLoadSilentAction } from './after-room-load-silent-action';
import { ObservableProperty } from '@wildebeest/observe-changes';

export class RoomRefreshSilentAction extends SubscriptionAction<any> {
    constructor(
        private roomApi: RoomApi,
        private roomStore: RoomStore,
        private roomFactory: RoomFactory,
        private dispatcher: EventDispatcher,
        private proxyLogger: ProxyLogger
    ) {
        super();
    }

    onActivate(result: any): void {
        let roomProperty: ObservableProperty<RoomEntity> = this.roomStore.active();
        if (roomProperty.isEmpty()) {
            return;
        }
        this.roomApi.load(roomProperty.get().getId())
            .subscribe(new AfterRoomLoadSilentAction(
                this.roomStore,
                this.roomFactory,
                this.dispatcher,
                this.proxyLogger
            ));
    }
}