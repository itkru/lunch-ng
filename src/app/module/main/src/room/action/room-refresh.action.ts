import { SubscriptionAction, EventDispatcher } from "src/app/module/dispatcher";
import { RoomApi } from "src/app/module/room/src/service/room.api";
import { RoomStore, RoomFactory, RoomEntity } from "src/app/module/room";
import { AfterRoomLoadAction } from "./after-room-load.action";
import { ProxyLogger } from "src/app/module/logger";
import { ComponentModelRepository } from 'src/app/module/gui/src/service/component-model.repository';
import { Closable, ObservableProperty } from '@wildebeest/observe-changes';

export class RoomRefreshAction extends SubscriptionAction<any> {
    constructor(
        private modelRepository: ComponentModelRepository,
        private roomApi: RoomApi,
        private roomStore: RoomStore,
        private roomFactory: RoomFactory,
        private dispatcher: EventDispatcher,
        private proxyLogger: ProxyLogger
    ) {
        super();
    }

    onActivate(result: any): void {
        let roomProperty: ObservableProperty<RoomEntity> = this.roomStore.active();
        if (roomProperty.isEmpty()) {
            return;
        }
        let loading: Closable = this.modelRepository.get('room:active').loading().start()
        this.roomApi.load(roomProperty.get().getId())
            .subscribe(new AfterRoomLoadAction(
                loading,
                this.roomStore,
                this.roomFactory,
                this.dispatcher,
                this.proxyLogger
            ));
    }
}