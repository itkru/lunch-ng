import { Event } from "src/app/lib/event/event";
import { RoomStore, RoomFactory } from "src/app/module/room";
import { EventDispatcher } from "src/app/module/dispatcher";
import { ProxyLogger } from "src/app/module/logger";
import { Closable } from '@wildebeest/observe-changes';

export class AfterRoomLoadAction {
    constructor(
        protected loading: Closable,
        protected roomStore: RoomStore,
        protected roomFactory: RoomFactory,
        protected dispatcher: EventDispatcher,
        protected proxyLogger: ProxyLogger
    ) {}

    next(result: any): void {
        this.roomStore.activate(
            this.roomFactory.create(result.room.id, result.room.name)
        );
        this.dispatcher.dispatch(
            new Event("activity.loaded", result.room.activities)
        );
    }

    error(error: any): void {
        this.loading.close();
        this.proxyLogger.alert(error.message, {exception: error});
        this.dispatcher.dispatch(
            new Event("toast.show", "Server is not responding properly")
        );
    }

    complete(): void {
        this.loading.close();
    }
}