import { RoomStore, RoomFactory, RoomEntity } from "src/app/module/room";
import { EventDispatcher } from "src/app/module/dispatcher";
import { ProxyLogger } from "src/app/module/logger";
import { AfterRoomLoadAction } from './after-room-load.action';
import { EmptyClosable } from '../../empty.closable';

export class AfterRoomLoadSilentAction extends AfterRoomLoadAction {
    constructor(
        protected roomStore: RoomStore,
        protected roomFactory: RoomFactory,
        protected dispatcher: EventDispatcher,
        protected proxyLogger: ProxyLogger
    ) {
        super(
            new EmptyClosable(),
            roomStore,
            roomFactory,
            dispatcher,
            proxyLogger
        );
    }

    error(error: any): void {
        this.proxyLogger.warning(error.message, {exception: error});
    }

    complete(): void {
        // Do not close loading
    }
}