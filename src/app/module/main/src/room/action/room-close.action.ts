import { SubscriptionAction, EventDispatcher } from "src/app/module/dispatcher";
import { RoomStore } from "src/app/module/room";
import { Event } from "src/app/lib/event/event";
import { ComponentModelRepository } from 'src/app/module/gui/src/service/component-model.repository';
import { AppStore } from '../../app.store';

export class RoomCloseAction extends SubscriptionAction<any> {
    constructor(
        private roomStore: RoomStore,
        private appStore: AppStore,
        private dispatcher: EventDispatcher,
        private modelRepository: ComponentModelRepository
    ) {
        super();
    }

    onActivate(): void {
        this.roomStore.deactivate();
        this.modelRepository.remove("room:active");
        this.appStore.placeAutocomplete().setAll([]);
        this.dispatcher.dispatch(
            new Event("activity.loaded", [])
        );
    }
}