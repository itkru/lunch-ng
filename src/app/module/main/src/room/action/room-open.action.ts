import { SubscriptionAction, EventDispatcher } from "src/app/module/dispatcher";
import { RoomApi } from "src/app/module/room/src/service/room.api";
import { RoomStore, RoomFactory } from "src/app/module/room";
import { AfterRoomLoadAction } from "./after-room-load.action";
import { ProxyLogger } from "src/app/module/logger";
import { ComponentModelRepository } from 'src/app/module/gui/src/service/component-model.repository';
import { Closable } from '@wildebeest/observe-changes';

export class RoomOpenAction extends SubscriptionAction<string> {
    constructor(
        private componentModelRepository: ComponentModelRepository,
        private roomApi: RoomApi,
        private roomStore: RoomStore,
        private roomFactory: RoomFactory,
        private dispatcher: EventDispatcher,
        private proxyLogger: ProxyLogger
    ) {
        super();
    }

    onActivate(roomName: string): void {
        let loading: Closable = this.componentModelRepository.get("room:active").loading().start()
        this.roomApi.create(roomName)
            .subscribe(new AfterRoomLoadAction(
                loading,
                this.roomStore,
                this.roomFactory,
                this.dispatcher,
                this.proxyLogger
            ));
    }
}