import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { EventDispatcher } from "src/app/module/dispatcher";
import { PlaceStore } from "src/app/module/place";
import { ActivityStore } from "src/app/module/activity";
import { GreetingsStore } from "src/app/module/greetings";
import { Injectable } from "@angular/core";
import { Event } from "src/app/lib/event/event";
import { TimeBlockStore } from '../time-block/time-block.store';
import { RoomStore } from 'src/app/module/room';

@Injectable()
export class RoomViewResolver implements Resolve<any> {
    constructor(
        private dispatcher: EventDispatcher,
        private placeStore: PlaceStore,
        private activityStore: ActivityStore,
        private greetingsStore: GreetingsStore,
        private roomStore: RoomStore,
        private timeBlockStore: TimeBlockStore
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        if (route.paramMap.has('placeId')) {
            let placeId: string = route.paramMap.get('placeId');
            this.dispatcher.dispatch(
                new Event("place.open", placeId)
            );
        } else {
            this.dispatcher.dispatch(
                new Event("place.close")
            );
        }
        return {
            place: this.placeStore.active(),
            greetings: this.greetingsStore.getList(),
            room: this.roomStore.active(),
            selectedActivity: this.activityStore.selected(),
            blocks: this.timeBlockStore.blocks()
        };
    }
}