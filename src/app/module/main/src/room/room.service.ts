import { EventDispatcher } from "src/app/module/dispatcher";
import { RoomApi } from "../../../room/src/service/room.api";
import { RoomStore } from "../../../room/src/service/room.store";
import { RoomFactory } from "src/app/module/room";
import { Injectable } from "@angular/core";
import { Event } from "src/app/lib/event/event";
import { PlaceApi, PlaceEntity, PlaceFactory } from "src/app/module/place";
import { OptionModel } from "src/app/module/gui";
import { RoomCloseAction } from "./action/room-close.action";
import { RoomOpenAction } from "./action/room-open.action";
import { RoomRefreshAction } from "./action/room-refresh.action";
import { RoomRefreshSilentAction } from "./action/room-refresh-silent.action";
import { ProxyLogger } from "src/app/module/logger";
import { GeoipStore, GeoipEntity } from 'src/app/module/geoip';
import { ComponentModelRepository } from 'src/app/module/gui/src/service/component-model.repository';
import { Closable } from '@wildebeest/observe-changes';
import { AppStore } from '../app.store';

@Injectable()
export class RoomService {
    constructor(
        private dispatcher: EventDispatcher,
        private roomApi: RoomApi,
        private roomStore: RoomStore,
        private roomFactory: RoomFactory,
        private placeApi: PlaceApi,
        private placeFactory: PlaceFactory,
        private proxyLogger: ProxyLogger,
        private geoipStore: GeoipStore,
        private componentModelRepository: ComponentModelRepository,
        private appStore: AppStore
    ) {}

    public start(): void {
        setInterval(() => {
            if (this.roomStore.active().isEmpty()) {
                return;
            }
            this.dispatcher.dispatch(
                new Event("room.refresh.silent")
            )
        }, 10000);

        this.dispatcher.observe("room.open")
            .subscribe(new RoomOpenAction(
                this.componentModelRepository,
                this.roomApi,
                this.roomStore,
                this.roomFactory,
                this.dispatcher,
                this.proxyLogger
            ));

        this.dispatcher.observe("room.close")
            .subscribe(new RoomCloseAction(
                this.roomStore,
                this.appStore,
                this.dispatcher,
                this.componentModelRepository
            ));

        this.dispatcher.observe("room.refresh")
            .subscribe(new RoomRefreshAction(
                this.componentModelRepository,
                this.roomApi,
                this.roomStore,
                this.roomFactory,
                this.dispatcher,
                this.proxyLogger
            ));

        this.dispatcher.observe("room.refresh.silent")
            .subscribe(new RoomRefreshSilentAction(
                this.roomApi,
                this.roomStore,
                this.roomFactory,
                this.dispatcher,
                this.proxyLogger
            ));

        this.dispatcher.observe("place.search")
            .subscribe((event) => {
                this.searchPlace(event.getValue().placeName, event.getValue().roomId);
            });
    }

    private searchPlace(placeName: string, roomId: string): void {
        let loading: Closable = this.componentModelRepository.get('place:search').loading().start();

        let geoipEntity: GeoipEntity = this.geoipStore.getUserGeoip().get();

        let geolocation: any = null;
        if (geoipEntity) {
            geolocation = {
                longitude: geoipEntity.getLongitude(),
                latitude: geoipEntity.getLatitude()
            };
        }

        this.placeApi.search(placeName, roomId, geolocation)
            .subscribe({
                next: (result) => {
                    let resultPlaces: any = result.places;
                    let options: OptionModel<PlaceEntity>[] = [];
                    for (let i = 0; i < resultPlaces.length; i++) {
                        options.push(
                            new OptionModel(
                                resultPlaces[i].name,
                                this.placeFactory.createOption(
                                    resultPlaces[i].id,
                                    resultPlaces[i].name,
                                    resultPlaces[i].external_id
                                ), {
                                    more: resultPlaces[i].location
                                }
                            )
                        )
                    }
                    this.appStore.placeAutocomplete()
                        .setAll(options);
                },
                error: (err: any) => {
                    loading.close();
                    this.proxyLogger.alert(err.message, {exception: err});
                    this.dispatcher.dispatch(new Event("toast.show", "Server is not responding properly"));
                },
                complete: () => {
                    loading.close();
                }
            });
    }
}