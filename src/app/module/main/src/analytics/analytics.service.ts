import { Injectable } from "@angular/core";
import { PageviewProxy } from "src/app/module/analytics";
import { Router, NavigationEnd } from "@angular/router";

@Injectable()
export class AnalyticsService {
    constructor(
        private pageviewProxy: PageviewProxy,
        private router: Router
    ) {}

    public start(): void {
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.pageviewProxy.pageview(event.urlAfterRedirects);
            }
        });
    }
}