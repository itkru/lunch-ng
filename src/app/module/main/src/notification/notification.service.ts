declare var Notification: any;
declare var Cookies: any;

import { Injectable } from "@angular/core";
import { NotificationFactory, NotificationEntity } from "src/app/module/browser-notification";
import { EventDispatcher } from "src/app/module/dispatcher";
import { Event } from "src/app/lib/event/event";
import { ComponentModelRepository } from 'src/app/module/gui/src/service/component-model.repository';
import { ComponentModel } from 'src/app/module/gui/src/model/component.model';

@Injectable()
export class NotificationService {
    private worker: ServiceWorkerRegistration;
    private model: ComponentModel;

    constructor(
        private dispatcher: EventDispatcher,
        private notificationFactory: NotificationFactory,
        private modelRepository: ComponentModelRepository
    ) {}

    public start(): void {
        this.model = this.modelRepository.get('browser-notification');
        this.startWorker();
        this.updateModel();

        this.dispatcher.observe("notification.requestPermission")
            .subscribe((event: Event) => {
                this.requestPermission();
            });

        this.dispatcher.observe("notification.show")
            .subscribe((event: Event) => {
                this.showNotification(event.getValue());
            });
    }

    private updateModel(): void {
        
        let canRequest: boolean = this.supportsNotification() && !this.isGranted() && !this.isDenied();
        this.model.attributes().set('canRequest', canRequest);
        this.model.attributes().set('highlight', Cookies.get('show_notification_tooltip'));
    }

    private supportsNotification(): boolean {
        return (typeof Notification !== 'undefined');
    }

    private isGranted(): boolean {
        return (this.supportsNotification() && Notification.permission === "granted");
    }

    private isDenied(): boolean {
        return (this.supportsNotification() && Notification.permission === "denied");
    }

    private startWorker(): void {
        if (!navigator || !navigator.serviceWorker) {
            return;
        }
        navigator.serviceWorker.register('sw.js', {scope: '/'})
            .then((sw: any) => {
                this.worker = sw;
            });
    }

    private requestPermission(): void {
        Cookies.remove('show_notification_tooltip');
        this.updateModel();
        if (!this.supportsNotification() || this.isGranted()) {
            return;
        }
        return Notification.requestPermission()
            .then((permission: string) => {
                this.updateModel()
                if (permission == "granted") {
                    this.dispatcher.dispatch(
                        new Event("notification.show", {
                            title: 'Výborne',
                            body: "Takéto upozornenie ti príde, keď ti začne obed."
                        })
                    );
                }
                return permission;
            });
    }

    private showNotification(notificationData: any): void {
        if (this.worker == null) {
            return;
        }
        if (!this.isGranted()) {
            if (this.model.attributes().get('canRequest')) {
                this.requestPermission();
            }
            return;
        }
        
        let notification: NotificationEntity = this.notificationFactory.builder()
            .withTitle(notificationData.title)
            .withBody(notificationData.body)
            .withImage("assets/images/ovaldo.png")
            .build();

        if (notification.getOptions().sound) {
            let player: any = new Audio();
            player.src = notification.getOptions().sound;
            player.load();
            player.play();
        }
        this.worker.showNotification(notification.getTitle(), notification.getOptions());
    }
}