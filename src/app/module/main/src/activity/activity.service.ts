import { EventDispatcher } from "src/app/module/dispatcher";
import { ActivityStore } from "../../../activity/src/service/activity.store";
import { ActivityApi } from "../../../activity/src/service/activity.api";
import { Injectable } from "@angular/core";
import { ActivityFactory, ActivityEntity } from "src/app/module/activity";
import { Router } from "@angular/router";
import { RoomStore, RoomEntity } from "src/app/module/room";
import { Event } from "src/app/lib/event/event";
import { GuestStore } from "src/app/module/guest";
import { ActivityAddAction } from "./action/activity-add.action";
import { ActivityJoinAction } from "./action/activity-join.action";
import { ActivityLeaveAction } from "./action/activity-leave.action";
import { ActivitySelectAction } from "./action/activity-select.action";
import { ProxyLogger } from "src/app/module/logger";
import { PlaceFactory } from 'src/app/module/place';
import { TranslateService } from '@ngx-translate/core';
import { MapEntry, ObservableMap, PropertyChange, Closable, MapChange } from '@wildebeest/observe-changes';
import { ComponentModelRepository } from 'src/app/module/gui/src/service/component-model.repository';
import { AppStore } from '../app.store';

@Injectable()
export class ActivityService {
    constructor(
        private appStore: AppStore,
        private dispatcher: EventDispatcher,
        private activityStore: ActivityStore,
        private roomStore: RoomStore,
        private activityApi: ActivityApi,
        private activityFactory: ActivityFactory,
        private router: Router,
        private guestStore: GuestStore,
        private proxyLogger: ProxyLogger,
        private placeFacory: PlaceFactory,
        private translateService: TranslateService,
        private modelRepository: ComponentModelRepository
) {}

    public start(): void {
        this.clearForm("");

        this.roomStore.active().addListenerAndCall(
            (change: PropertyChange<RoomEntity>) => {
                let room: RoomEntity = change.next();
                if (!room) {
                    return;
                }
                this.appStore.stepActivity().get()
                    .roomId = room.getId();
            }
        );

        this.activityStore.activities().addListenerAndCall(
            this.onActivitiesChange.bind(this)
        );

        this.activityStore.selected().addListenerAndCall(
            this.onActivitySelected.bind(this)
        );

        this.dispatcher.observe("activity.loaded")
            .subscribe((event: Event) => {
                this.loadedActivities(event.getValue());
            });

        this.dispatcher.observe("activity.save")
            .subscribe((event: Event) => {
                this.saveActivity(event.getValue());
            });

        this.dispatcher.observe("activity.create")
            .subscribe((event: Event) => {
                let activity: any = event.getValue();
                this.saveActivity(
                    this.activityFactory.create(
                        "",
                        this.roomStore.active().get().getId(),
                        activity.startAt,
                        this.placeFacory.createByObject({
                            id: activity.place.getId(),
                            name: activity.place.getName(),
                            external_id: activity.place.getExternalId()
                        }),
                        activity.text,
                        []
                    )
                );
            });

        this.dispatcher.observe("activity.add")
            .subscribe(new ActivityAddAction(
                this.dispatcher,
                this.activityStore
            ));

        this.dispatcher.observe("activity.join")
            .subscribe(new ActivityJoinAction(
                this.modelRepository,
                this.activityApi,
                this.dispatcher,
                this.proxyLogger
            ));

        this.dispatcher.observe("activity.leave")
            .subscribe(new ActivityLeaveAction(
                this.modelRepository,
                this.activityApi,
                this.dispatcher,
                this.proxyLogger
            ));

        this.dispatcher.observe("activity.select")
            .subscribe(new ActivitySelectAction(
                this.activityStore
            ));

        this.dispatcher.observe('activity.form.clear')
            .subscribe((event: Event) => {
                this.clearForm(event.getValue());
            });
    }

    private clearForm(roomId: string) {
        this.appStore.clearStepActivity();
        this.appStore.stepActivity().get().roomId = roomId;
        this.appStore.placeAutocomplete().clear();
    }

    private loadedActivities(items: any[]): void {
        let activities: ObservableMap<string, ActivityEntity> = this.activityStore.activities();
        let models: MapEntry<string, ActivityEntity>[] = [];
        for (let item of items) {
            let activityEntity: ActivityEntity = this.activityFactory.createByObject(item);
            let originalActivity: ActivityEntity = activities.get(activityEntity.getId());
            if (originalActivity !== null && originalActivity.equals(activityEntity)) {
                activityEntity = originalActivity;
            }
            models.push(new MapEntry(activityEntity.getId(), activityEntity));
        }

        activities.setList(models);
    }

    private onActivitiesChange(change: MapChange<string, ActivityEntity>): void {
        let removedKeys: string[] = [];
        for (let removed of change.removed()) {
            removedKeys.push(removed.getKey());
            if (this.activityStore.selected().get() === removed.getValue()) {
                this.activityStore.deselect();
            }
        }

        let currentUser: string = "";
        if (!this.guestStore.authUser().isEmpty()) {
            currentUser = this.guestStore.authUser().get().getId()
        }
        let toSelect: ActivityEntity = null;
        for (let inserted of change.inserted()) {
            if (inserted.getValue().hasUser(currentUser)) {
                toSelect = inserted.getValue();
            }
            let removedKeysIndex: number = removedKeys.indexOf(inserted.getKey());
            if (removedKeysIndex > -1) {
                removedKeys.splice(removedKeysIndex, 1);
            }
        }
        if (toSelect !== null) {
            this.activityStore.select(toSelect);
        }

        for (let removeModel of removedKeys) {
            this.modelRepository.remove("activity:" + removeModel);
        }
    }

    private onActivitySelected(change: PropertyChange<ActivityEntity>): void {
        if (change.previous() !== null) {
            this.modelRepository
                .get("activity:" + change.previous().getId())
                .attributes()
                .disable('selected');
        }

        if (change.next() !== null) {
            this.modelRepository
                .get("activity:" + change.next().getId())
                .attributes()
                .enable('selected');
        }
    }

    private saveActivity(activity: ActivityEntity): void {
        let loading: Closable = this.modelRepository.get("room:active").loading().start();
        this.activityApi.create(activity)
            .subscribe({
                next: (result) => {
                    this.dispatcher.dispatch(
                        new Event(
                            "toast.show",
                            this.translateService.instant('step.create-toast')
                        )
                    );
                    this.dispatcher.dispatch(
                        new Event("activity.add", this.activityFactory.createByObject(result.activity))
                    );
                    this.router.navigate([
                        this.roomStore.getActive().get().getName()
                    ]);
                    this.clearForm(
                        this.roomStore.getActive().get().getId()
                    );
                },
                error: (err: any) => {
                    loading.close();
                    this.proxyLogger.emergency(err.message, {exception: err});
                    this.dispatcher.dispatch(
                        new Event(
                            "toast.show",
                            this.translateService.instant('step.create-error-toast')    
                        )
                    );
                },
                complete: () => {
                    loading.close();
                }
            });
    }
}