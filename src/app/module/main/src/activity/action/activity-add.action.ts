import { ActivityEntity, ActivityStore } from "src/app/module/activity";
import { EventDispatcher, SubscriptionAction } from "src/app/module/dispatcher";
import { ObservableMap } from '@wildebeest/observe-changes';

export class ActivityAddAction extends SubscriptionAction<ActivityEntity> {
    constructor(
        private dispatcher: EventDispatcher,
        private activityStore: ActivityStore
    ) {
        super();
    }

    onActivate(activity: ActivityEntity): void {
        let activities: ObservableMap<String, ActivityEntity> = this.activityStore.activities();
        activities.add(activity.getId(), activity);
    }
}