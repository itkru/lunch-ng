import { SubscriptionAction } from "src/app/module/dispatcher";
import { ActivityEntity, ActivityStore } from "src/app/module/activity";

export class ActivitySelectAction extends SubscriptionAction<ActivityEntity> {
    constructor(
        private activityStore: ActivityStore
    ) {
        super();
    }

    onActivate(value: ActivityEntity): void {
        this.activityStore.select(value);
    }
}