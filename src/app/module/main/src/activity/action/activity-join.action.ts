import { ActivityApi, ActivityEntity } from "src/app/module/activity";
import { Event } from "src/app/lib/event/event";
import { EventDispatcher, SubscriptionAction } from "src/app/module/dispatcher";
import { ProxyLogger } from "src/app/module/logger";
import { ComponentModelRepository } from 'src/app/module/gui/src/service/component-model.repository';
import { Closable } from '@wildebeest/observe-changes';

export class ActivityJoinAction extends SubscriptionAction<ActivityEntity> {
    constructor(
        private modelRepository: ComponentModelRepository,
        private activityApi: ActivityApi,
        private dispatcher: EventDispatcher,
        private proxyLogger: ProxyLogger
    ) {
        super();
    }

    onActivate(activity: ActivityEntity): void {
        let loading: Closable = this.modelRepository.get("room:active").loading().start();
        this.activityApi.join(activity)
            .subscribe({
                next: (result) => {
                    this.dispatcher.dispatch(
                        new Event("room.refresh")
                    );
                },
                error: (error: any) => {
                    loading.close();
                    this.proxyLogger.emergency(error.message, {exception: error});
                },
                complete: () => {
                    loading.close();
                }
            });
    }
}