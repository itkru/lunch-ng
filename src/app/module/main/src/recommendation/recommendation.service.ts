import { Injectable } from '@angular/core';
import { RoomStore, RoomEntity, RoomApi } from 'src/app/module/room';
import { RecommendationStore } from './recommendation.store';
import { subDays, format } from 'date-fns';
import { PlaceStore, PlaceEntity, PlaceFactory } from 'src/app/module/place';
import { PropertyChange } from '@wildebeest/observe-changes';

@Injectable()
export class RecommendationService {
    constructor(
        private roomApi: RoomApi,
        private roomStore: RoomStore,
        private recommendationStore: RecommendationStore,
        private placeStore: PlaceStore,
        private placeFactory: PlaceFactory
    ) {}

    public start(): void {
        this.roomStore.active().addListenerAndCall(
            this.onRoomChange.bind(this)
        );
    }

    private onRoomChange(change: PropertyChange<RoomEntity>): void {
        let room: RoomEntity = change.next();
        if (room === null || room === undefined) {
            this.recommendationStore.recommendations().clear();
            return;
        }

        for (let i of [1, 2, 7]) {
            let date: Date = subDays(new Date(), i);
            
            this.roomApi.loadFilter(room.getId(), format(date, "yyyy-MM-dd"))
                .subscribe(this.onLoadActivities.bind(this));
        }
    }

    private onLoadActivities(result: any): void {
        for (let activity of result.room.activities) {
            let time: string = format(new Date(activity.starts_at), "HH:mm");

            let placeId: string = activity.place.id;
            let newPlace: PlaceEntity = this.placeFactory.createByObject(activity.place);
            if (this.placeStore.places().containes(placeId)) {
                let place: PlaceEntity = this.placeStore.places().get(placeId);
                if (place.getId() === newPlace.getId()) {
                    newPlace = place;
                }
            }

            this.placeStore.places().add(placeId, newPlace);
            this.recommendationStore.recommendations().add(time, newPlace);
        }
    }
}
