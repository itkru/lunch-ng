import { Injectable } from '@angular/core';
import { PlaceEntity } from 'src/app/module/place';
import { ObservableGroup, SimpleObservableGroup } from '@wildebeest/observe-changes';

@Injectable()
export class RecommendationStore {
    private recommendationsValue: ObservableGroup<string, PlaceEntity>;

    constructor() {
        this.recommendationsValue = new SimpleObservableGroup();
    }

    public recommendations(): ObservableGroup<string, PlaceEntity> {
        return this.recommendationsValue;
    }
}
