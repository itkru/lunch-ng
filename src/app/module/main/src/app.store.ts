import { ObservableProperty, SimpleObservableProperty, ObservableList, SimpleObservableList } from '@wildebeest/observe-changes';
import { Injectable } from '@angular/core';
import { GuestEntity } from '../../guest';
import { OptionModel } from '../../gui';

@Injectable()
export class AppStore {
    private languageValue: ObservableProperty<string>;
    private stepActivityValue: ObservableProperty<any>;
    private userValue: ObservableProperty<GuestEntity>;
    private placeAutocompleteValue: ObservableList<OptionModel<any>>;

    public constructor() {
        this.languageValue = new SimpleObservableProperty();
        this.stepActivityValue = new SimpleObservableProperty();
        this.userValue = new SimpleObservableProperty();
        this.placeAutocompleteValue = new SimpleObservableList();
    }

    public user(): ObservableProperty<GuestEntity> {
        return this.userValue;
    }

    public language(): ObservableProperty<string> {
        return this.languageValue;
    }

    public stepActivity(): ObservableProperty<any> {
        return this.stepActivityValue;
    }

    public placeAutocomplete(): ObservableList<OptionModel<any>> {
        return this.placeAutocompleteValue;
    }

    public clearStepActivity(): void {
        let startsAt: Date = new Date();
        startsAt.setMinutes(0);
        startsAt.setSeconds(0);
        this.stepActivity().set({
            roomId: "",
            startAt: startsAt,
            text: "",
            place: {
                name: "",
                id: null
            },
            users: []
        });
    }
}