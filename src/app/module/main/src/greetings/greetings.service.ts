import { Injectable } from '@angular/core';
import { GreetingsStore } from 'src/app/module/greetings';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class GreetingsService {
    constructor(
        private greetingsStore: GreetingsStore,
        private translateService: TranslateService,
    ) {}

    public start(): void {
        this.translateService.stream('room.no-activity-messages')
            .subscribe((messages: string[]) => {
                if (!messages) {
                    messages = [];
                }
                this.greetingsStore.getList().setAll(messages);
            });
    }
}