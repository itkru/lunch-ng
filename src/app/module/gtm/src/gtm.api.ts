import { PageviewAnalytics } from "../../analytics";

declare var dataLayer: any;

export class GtmApi implements PageviewAnalytics {
    constructor(config: any) {
        (function(w:any,d:any,s:any,l:any,i:any){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth=baif5-j5uUpzoGJjwyzblg&gtm_preview=env-2&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer',config.id);
    }

    public pageview(pageUrl: string): void {
        dataLayer.push({
            event: 'pageview',
            page: {
              path: pageUrl
            }
          });
    }
}