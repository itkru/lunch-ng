import { NgModule } from '@angular/core';
import { TextFormaterService } from './service/text-formater.service';
import { FormatTextContainerDirective } from './directive/format-text-container.directive';
import { FormatTextComponent } from './component/format-text.component';
import { FormatLinkComponent } from './component/format-link.component';
import { FormatPlainComponent } from './component/format-plain.component';

@NgModule({
    imports: [],
    exports: [FormatTextComponent],
    declarations: [FormatTextContainerDirective, FormatTextComponent, FormatLinkComponent, FormatPlainComponent],
    providers: [TextFormaterService],
    entryComponents: [FormatPlainComponent, FormatLinkComponent]
})
export class TemplateModule {
    constructor () {
        
    }
}
