import { Type } from '@angular/core';

export class TextFormat {

    private type: Type<any>;
    private inputs: any;
    private plain: boolean;

    constructor(componentType: Type<any>, inputs: any, plain: boolean = false) {
        this.type = componentType;
        this.inputs = inputs;
        this.plain = plain;
    }

    getType(): Type<any> {
        return this.type;
    }

    getInputs(): any {
        return this.inputs;
    }

    isPlain(): boolean {
        return this.plain;
    }

    getText(): string {
        return this.inputs.text;
    }
}