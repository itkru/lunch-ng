import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'kruFormatPlain',
    templateUrl: 'format-plain.component.html'
})

export class FormatPlainComponent implements OnInit {

    @Input('text')
    public text: string;
    
    constructor() { }

    ngOnInit() { }
}