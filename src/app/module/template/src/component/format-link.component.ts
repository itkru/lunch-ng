import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'kruFormatLink',
    templateUrl: 'format-link.component.html'
})

export class FormatLinkComponent implements OnInit {

    @Input('text')
    public text: string;
    @Input('link')
    public link: string;
    
    constructor() { }

    ngOnInit() { }
}