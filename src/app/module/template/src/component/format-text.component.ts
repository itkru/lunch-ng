import { Component, OnInit, AfterViewInit, Input, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentRef } from '@angular/core';

import { FormatTextContainerDirective } from '../directive/format-text-container.directive';
import { TextFormaterService } from '../service/text-formater.service';

@Component({
    selector: 'kruFormatText',
    templateUrl: 'format-text.component.html',
    styleUrls: ['format-text.component.css']
})

export class FormatTextComponent implements OnInit, AfterViewInit {

    @Input('kruText')
    public text: string = "";
    public components: any = [];
    @ViewChild(FormatTextContainerDirective, {static: false})
    public containerDirective: FormatTextContainerDirective;

    private container: ViewContainerRef;

    constructor(private componentFactoryResolver: ComponentFactoryResolver, private textFormaterService: TextFormaterService) {

    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.container = this.containerDirective.viewContainerRef;
            this.container.clear();

            for (let i: number = 0; i < this.components.length; i++) {
                let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.components[i].getType());
                let componentRef: ComponentRef<any> = this.container.createComponent(componentFactory);
                let inputs: any = this.components[i].getInputs();
                for (let key in inputs) {
                    componentRef.instance[key] = inputs[key];
                }
            }
        });
    }

    ngOnInit() { }

    ngOnChanges(): void {
        this.components = this.getTextComponent(this.text);
    }

    public getTextComponent(text: string): any[] {
        let components: any[] = [this.textFormaterService.makePlain(text)];
        components = this.textFormaterService.detectLinks(components);
        return components;
    }
}