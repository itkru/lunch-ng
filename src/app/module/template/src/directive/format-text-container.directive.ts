import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[kruFormatTextContainer]',
})
export class FormatTextContainerDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}