import { Injectable } from '@angular/core';

import { TextFormat } from '../object/text-format.object';
import { FormatPlainComponent } from '../component/format-plain.component';
import { FormatLinkComponent } from '../component/format-link.component';

@Injectable()
export class TextFormaterService {

    constructor() {

    }

    detectLinks(components: any[]): any[]  {
        let newComponents: any[] = [];
        var exp = /(\bhttps?:\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        for (let i: number = 0; i < components.length; i++) {
            if (!components[i].isPlain()) {
                newComponents.push(components[i]);
                continue;
            }
            let matches: any = components[i].getText().match(exp);
            if (!matches) {
                newComponents.push(components[i]);
            } else {
                let text: string = components[i].getText();
                for (let j: number = 0; j < matches.length; j++) {
                    let startPosition = text.indexOf(matches[j]);
                    if (startPosition == -1) {
                        continue;
                    }
                    if (startPosition > 0) {
                        newComponents.push(new TextFormat(FormatPlainComponent, {text: text.substr(0, startPosition)}, true));
                    }

                    let link: string = text.substr(startPosition, matches[j].length);
                    newComponents.push(new TextFormat(FormatLinkComponent, {text: link, link: link}));
                    text = text.substr(startPosition + matches[j].length);
                }
                if (text.length > 0) {
                    newComponents.push(new TextFormat(FormatPlainComponent, {text: text}, true));
                }
            }
        }
        return newComponents;
    }

    format(text: string): string {
        text = text.replace(/{\[#\]/g, '<span class="highlight-word">');
        text = text.replace(/\[#\]}/g, '</span>');
        return text;
    }

    makePlain(text: string): any {
        return new TextFormat(FormatPlainComponent, {text: text}, true);
    }
}