import { NgModule } from "@angular/core";
import { ToastService } from "./toast.service";
import { ToastStore } from "./toast.store";

@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [ToastService, ToastStore]
})
export class ToastModule {
    
}