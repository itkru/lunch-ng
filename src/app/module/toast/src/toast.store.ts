import { Injectable } from "@angular/core";
import { ObservableList, SimpleObservableList } from '@wildebeest/observe-changes';
import { ToastEntity } from './toast.entity';

@Injectable()
export class ToastStore {
    private list: ObservableList<ToastEntity>;

    constructor() {
        this.list = new SimpleObservableList();
    }

    public getList(): ObservableList<ToastEntity> {
        return this.list;
    }
}