export class ToastEntity {
    public constructor(
        private message: string
    ) {}
    
    public getMessage(): string {
        return this.message;
    }
}