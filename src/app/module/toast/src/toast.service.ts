import { ToastStore } from './toast.store';
import { EventDispatcher } from 'src/app/module/dispatcher';
import { Injectable } from '@angular/core';
import { Event } from 'src/app/lib/event/event';
import { ToastEntity } from './toast.entity';

@Injectable()
export class ToastService {
    constructor(
        private toastStore: ToastStore,
        private eventDispatcher: EventDispatcher
    ) {}

    public start(): void {
        this.eventDispatcher.observe("toast.show")
            .subscribe((event: Event) => {
                this.add(event.getValue())
            });
    }

    private add(text: string): void {     
        if (text == null) {
            return;
        }
        let toast: ToastEntity = new ToastEntity(text);
        this.toastStore.getList().add(toast);
        this.timeoutAutohide(toast);
    }

    private timeoutAutohide(toast: ToastEntity): void {
        setTimeout(() => {
            this.toastStore.getList().remove(toast);
        }, 3000);
    }
}