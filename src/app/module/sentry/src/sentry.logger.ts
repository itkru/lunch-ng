import { AbstractLogger } from "../../logger";
import * as Sentry from '@sentry/browser';

export class SentryLogger extends AbstractLogger {
    private levelMapping: any = {
        debug: Sentry.Severity.Debug,
        info: Sentry.Severity.Info,
        notice: Sentry.Severity.Info,
        warning: Sentry.Severity.Warning,
        error: Sentry.Severity.Error,
        alert: Sentry.Severity.Error,
        critical: Sentry.Severity.Critical,
        emergency: Sentry.Severity.Fatal
    };

    constructor(config: any) {
        super();
        Sentry.init({
            'dsn': config.dsn,
            'environment': config.environment
          });
    }

    log(type: string, message: string, context): void {
        Sentry.configureScope((scope) => {
            scope.setExtras(context);
        });
        Sentry.captureMessage(message, this.levelMapping[type]);
    }
}