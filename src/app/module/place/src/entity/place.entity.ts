export class PlaceEntity {
    constructor(
        protected id: string | null,
        protected name: string,
        protected externalId: string | null,
        protected dailyMenu: any
    ) {}

    public getId(): string | null {
        return this.id;
    }

    public getName(): string {
        return this.name;
    }

    public getExternalId(): string | null{
        return this.externalId;
    }

    public getDailyMenu(): any {
        return this.dailyMenu;
    }

    public hasDailyMenu(): boolean {
        return this.dailyMenu !== null && this.dailyMenu !== undefined;
    }

    public equals(place: PlaceEntity): boolean {
        return this.getId() === place.getId()
            && this.getName() === place.getName()
            && this.getExternalId() === place.getExternalId()
            && JSON.stringify(this.getDailyMenu()) === JSON.stringify(place.getDailyMenu());
    }
}