import { PlaceEntity } from "./place.entity";
import { Injectable } from "@angular/core";

@Injectable()
export class PlaceFactory {
    public create(id: string | null, name: string, externalId: string | null, dailyMenu: any): PlaceEntity {
        return new PlaceEntity(id + "", name, externalId, dailyMenu);
    }

    public createByObject(data: any): PlaceEntity {
        let menu: any = null;
        if (data.menu && data.menu.length > 0 && data.menu[0].daily_menu) {
            menu = data.menu[0].daily_menu;
        }
        return this.create(
            data.id || null,
            data.name,
            data.external_id || null,
            menu
        );
    }

    public createOption(id, name, externalId): PlaceEntity {
        return this.create(id, name, externalId, null);
    }
}