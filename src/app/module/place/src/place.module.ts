import { NgModule } from '@angular/core';
import { PlaceFactory } from './entity/place.factory';
import { PlaceStore } from './service/place.store';
import { PlaceApi } from './service/place.api';

@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [PlaceFactory, PlaceStore, PlaceApi],
})
export class PlaceModule { 
    
}
