import { PlaceEntity } from "../entity/place.entity";
import { Injectable } from "@angular/core";
import { ObservableMap, SimpleObservableMap, ObservableProperty, SimpleObservableProperty } from '@wildebeest/observe-changes';

@Injectable()
export class PlaceStore {
    private activeValue: ObservableProperty<PlaceEntity>;
    private placesValue: ObservableMap<string, PlaceEntity>;

    constructor() {
        this.activeValue = new SimpleObservableProperty();
        this.placesValue = new SimpleObservableMap();
    }

    public places(): ObservableMap<string, PlaceEntity> {
        return this.placesValue;
    }

    public active(): ObservableProperty<PlaceEntity> {
        return this.activeValue;
    }

    public activate(place: PlaceEntity): void {
        this.activeValue.set(place);
    }

    public dectivate(): void {
        this.activeValue.set(null);
    }
}