import { Injectable } from "@angular/core";
import { RequestSender, RequestFactory, RequestBuilder } from "src/app/module/request";
import { Observable } from "rxjs";

@Injectable()
export class PlaceApi {
    constructor(
        private requestSender: RequestSender,
        private requestFactory: RequestFactory
    ) {}

    public search(placeName: string, roomId: string, geolocation?: any): Observable<any> {
        let requestBuiler: RequestBuilder = this.requestFactory.createBuilder()
            .withMethod('GET')
            .withPath('/places')
            .withQuery('search', placeName)
            .withQuery('room_id', roomId);

        if (geolocation) {
            requestBuiler.withQuery('lon', geolocation.longitude)
                .withQuery('lat', geolocation.latitude);
        }

        return this.requestSender.send(requestBuiler.build());
    }
}