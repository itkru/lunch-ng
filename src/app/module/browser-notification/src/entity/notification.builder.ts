import { NotificationEntity } from './notification.entity'

export class NotificationBuilder {
    private title: string = "";
    private options: any = {};

    public withTitle(title: string): NotificationBuilder {
        this.title = title;
        return this;
    }

    public withBody(body: string): NotificationBuilder {
        this.options.body = body;
        return this;
    }

    public withImage(image: string): NotificationBuilder {
        this.options.icon = image;
        return this;
    }

    public withSound(sound: string): NotificationBuilder {
        this.options.sound = sound;
        return this;
    }

    public withCallback(callback: any): NotificationBuilder {
        this.options.onClick = callback;
        return this;
    }

    public build(): NotificationEntity {
        return new NotificationEntity(
            this.title,
            this.options
        );
    }
}