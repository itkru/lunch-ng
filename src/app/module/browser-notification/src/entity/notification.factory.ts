import { Injectable } from "@angular/core";
import { NotificationBuilder } from "./notification.builder";

@Injectable()
export class NotificationFactory {
    public builder(): NotificationBuilder {
        return new NotificationBuilder();
    }
}