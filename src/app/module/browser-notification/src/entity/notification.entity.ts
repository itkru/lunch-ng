export class NotificationEntity {
    constructor(
        private title: string,
        private options: any,
    ) {}

    getTitle(): string {
        return this.title;
    }

    getOptions(): any {
        return this.options;
    }
}
