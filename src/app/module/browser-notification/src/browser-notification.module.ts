import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationFactory } from './entity/notification.factory';

@NgModule({
    imports: [ CommonModule ],
    declarations: [],
    exports: [],
    providers: [NotificationFactory]
})
export class BrowserNotificationModule {

}
