export { AnalyticsModule } from './src/analytics.module';

export { PageviewAnalytics } from './src/service/pageview.analytics';
export { PageviewProxy } from './src/service/pageview.proxy';