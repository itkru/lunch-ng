import { NgModule } from '@angular/core';
import { PageviewProxy } from './service/pageview.proxy';

@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [PageviewProxy],
})
export class AnalyticsModule { }
