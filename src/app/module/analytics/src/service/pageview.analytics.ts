export interface PageviewAnalytics {
    pageview(pageUrl: string): void;
}