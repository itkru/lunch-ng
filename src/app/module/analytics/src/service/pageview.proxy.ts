import { PageviewAnalytics } from "./pageview.analytics";
import { PageviewDummy } from "./pageview.dummy";
import { Injectable } from "@angular/core";

@Injectable()
export class PageviewProxy implements PageviewAnalytics {
    private proxy: PageviewAnalytics;

    constructor() {
        this.proxy = new PageviewDummy();
    }

    setProxy(proxy: PageviewAnalytics): void {
        this.proxy = proxy;
    }

    pageview(pageUrl: string): void {
        this.proxy.pageview(pageUrl);
    }
}