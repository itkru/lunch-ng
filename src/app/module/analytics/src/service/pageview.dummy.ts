import { PageviewAnalytics } from "./pageview.analytics";

export class PageviewDummy implements PageviewAnalytics {
    pageview(pageUrl: string): void {
        console.log("pageview", pageUrl);
    }
}