import { RequestEntity } from "./request.entity";
import { HttpHeaders } from '@angular/common/http';

export class RequestBuilder {
    private method: string;
    private url: string;
    private base: string;
    private path: string;
    private data: any;
    private headers: HttpHeaders;
    private headerOptions: any = {
        withCredentials: true
    };
    private queries: any = {};

    constructor() {
        this.method = "GET";
        this.headers = new HttpHeaders();
    }

    public withMethod(method: string): RequestBuilder {
        this.method = method;
        return this;
    }

    public withUrl(url: string): RequestBuilder {
        this.url = url;
        return this;
    }

    public withBase(base: string): RequestBuilder {
        this.url = "";
        this.base = base;
        return this;
    }

    public withPath(path: string): RequestBuilder {
        this.url = "";
        this.path = path;
        return this;
    }

    public withData(data: any): RequestBuilder {
        this.data = data;
        return this;
    }

    public withoutCredentials(): RequestBuilder {
        this.headerOptions['withCredentials'] = false;
        return this;
    }

    public withValue(name: string, data: any): RequestBuilder {
        this.data[name] = data;
        return this;
    }

    public withHeaderOption(name: string, value: any): RequestBuilder {
        this.headerOptions[name] = value;
        return this
    }

    public withHeader(name: string, value: any): RequestBuilder {
        this.headers = this.headers.set(name, value);
        return this
    }

    public withQuery(name: string, value: string): RequestBuilder {
        this.queries[name] = value;
        return this;
    }

    private getQueryString(): string {
        let result: string[] = [];

        for (let name in this.queries) {
            result.push(name + "=" + encodeURI(this.queries[name]));
        }
        return result.join("&");
    }

    private getFinalUrl(): string {
        let queryString: string = this.getQueryString();
        if (queryString.length > 0) {
            queryString = "?" + queryString;
        }

        let url: string = this.base + this.path;
        if (this.url != "") {
            url = this.url;
        }
        return url + queryString;
    }

    public build(): RequestEntity {
        this.headerOptions.headers = this.headers;
        return new RequestEntity(
            this.getFinalUrl(),
            this.method,
            this.data,
            this.headerOptions
        );
    }
}
