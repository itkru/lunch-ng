import { Injectable } from "@angular/core";
import { RequestBuilder } from "./request.builder";

@Injectable()
export class RequestFactory {
    private httpBase: string;

    constructor() {}

    public setBase(base: string): void {
        this.httpBase = base;
    }

    public createBuilder(): RequestBuilder {
        let builder: RequestBuilder = new RequestBuilder();
        return builder.withBase(this.httpBase);
    }
}