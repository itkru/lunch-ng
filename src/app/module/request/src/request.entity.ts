export class RequestEntity {
    constructor(
        private url: string,
        private method: string,
        private data: any,
        private headers: any
    ) {}

    getMethod(): string {
        return this.method;
    }

    getUrl(): string {
        return this.url;
    }

    getData(): any {
        return this.data;
    }

    getHeaders(): any {
        return this.headers;
    }
}
