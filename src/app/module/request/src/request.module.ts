import { NgModule } from "@angular/core";
import { RequestSender } from "./request.sender";
import { RequestFactory } from "./request.factory";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
    imports: [HttpClientModule],
    exports: [],
    declarations: [],
    providers: [RequestSender, RequestFactory]
})
export class RequestModule {

}