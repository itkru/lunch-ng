import { Injectable } from "@angular/core";
import { RequestEntity } from "./request.entity";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class RequestSender {
    constructor(
        private http: HttpClient
    ) {}

    public send(request: RequestEntity): Observable<any> {
        if (request.getMethod() == "POST") {
            return this.http.post(request.getUrl(), request.getData(), request.getHeaders());
        } else {
            return this.http.get(request.getUrl(), request.getHeaders());
        }
    }
}