import { ActivityEntity } from 'src/app/module/activity';
import { ObservableList } from '@wildebeest/observe-changes';
import { PlaceEntity } from 'src/app/module/place';

export class TimeBlockEntity {
    public constructor(
        private time: Date,
        private activitiesValue: ObservableList<ActivityEntity>,
        private recommendationsValue: ObservableList<PlaceEntity>
    ) {}

    public activities(): ObservableList<ActivityEntity> {
        return this.activitiesValue;
    }

    public recommendations(): ObservableList<PlaceEntity> {
        return this.recommendationsValue;
    }

    public getTime(): Date {
        return this.time;
    }
}