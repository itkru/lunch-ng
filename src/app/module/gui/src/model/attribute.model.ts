import { ObservableProperty, SimpleObservableProperty } from '@wildebeest/observe-changes';


export class AttributeModel {
    private attributes: Map<string, ObservableProperty<any>>;

    constructor() {
        this.attributes = new Map();
    }

    attribute(name: string): ObservableProperty<any> {
        if (!this.attributes.has(name)) {
            this.attributes.set(name, new SimpleObservableProperty(false));
        }
        return this.attributes.get(name);
    }

    getAttribute(name: string): boolean {
        return this.attribute(name).get();
    }

    setAttribute(name: string, value: any): void {
        this.attribute(name).set(value);
    }

    enable(name: string): void {
        this.setAttribute(name, true);
    }

    disable(name: string): void {
        this.setAttribute(name, false);
    }

    toggle(name: string): void {
        this.setAttribute(name, !this.getAttribute(name));
    }
}