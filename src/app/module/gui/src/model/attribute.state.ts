import { ObservableProperty, SimpleObservableProperty } from '@wildebeest/observe-changes';

export class AttributeState {
    private values: Map<string, ObservableProperty<any>>;

    constructor() {
        this.values = new Map();
    }

    public property<T>(name: string): ObservableProperty<T> {
        if (!this.values.has(name)) {
            this.values.set(name, new SimpleObservableProperty());
        }
        return this.values.get(name);
    }

    public get<T>(name: string): T {
        return this.property<T>(name).get();
    }

    public set(name: string, value: any): void {
        this.property(name).set(value);
    }

    public enable(name: string): void {
        this.set(name, true);
    }

    public disable(name: string): void {
        this.set(name, false);
    }

    public toggle(name: string): void {
        this.set(name, !this.get(name));
    }
}