import { Observable, BehaviorSubject } from 'rxjs';
import { Closable, ObservableProperty, SimpleObservableProperty, PropertyChange, ObservableList, SimpleObservableList, ListChange } from '@wildebeest/observe-changes';

export class LoadingState {
    private running: ObservableList<number>;
    private property: ObservableProperty<boolean>;
    private subject: BehaviorSubject<boolean>;

    public constructor() {
        this.subject = new BehaviorSubject(false);

        this.property = new SimpleObservableProperty();
        this.property.addListener((change: PropertyChange<boolean>) => {
            this.subject.next(change.next());
        });

        this.running = new SimpleObservableList();
        this.running.addListener((change: ListChange<number>) => {
            this.property.set(!this.running.isEmpty());
        });
    }

    public observable(): Observable<boolean> {
        return this.subject.asObservable();
    }

    public is(): boolean {
        return this.subject.getValue();
    }

    public start(): Closable {
        let processId: number = (new Date()).getTime();
        this.running.add(processId);
        return new LoadingClosable(this, processId);
    }

    public stop(procesId: number): void {
        this.running.remove(procesId);
    }
}

class LoadingClosable implements Closable {
    public constructor(
        private loadingState: LoadingState,
        private processId: number
    ) {}

    close(): void {
        this.loadingState.stop(this.processId);
    }
}