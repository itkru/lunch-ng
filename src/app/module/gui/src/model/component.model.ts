import { AttributeState } from './attribute.state';
import { LoadingState } from './loading.state';
import { EventDispatcher } from 'src/app/module/dispatcher';

export class ComponentModel {
    private attributesValue: AttributeState;
    private loadingValue: LoadingState;
    private dispatcherValue: EventDispatcher;

    constructor() {
        this.attributesValue = new AttributeState();
        this.loadingValue = new LoadingState();
        this.dispatcherValue = new EventDispatcher();
    }

    public attributes(): AttributeState {
        return this.attributesValue;
    }

    public loading(): LoadingState {
        return this.loadingValue;
    }

    public dispatcher(): EventDispatcher {
        return this.dispatcherValue;
    }
}