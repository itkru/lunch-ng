import { AttributeModel } from "./attribute.model";

export class OptionModel<T> extends AttributeModel {
    private value: T;
    private name: string;
    private options: any;

    constructor(name: string, value: T, options: any) {
        super();
        this.name = name;
        this.value = value;
        this.options = options;
    }

    public getValue(): T {
        return this.value;
    }

    public getName(): string {
        return this.name;
    }

    public getOptions(): any {
        return this.options;
    }
}