import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'kruToast',
    templateUrl: 'toast.component.html'
})

export class ToastComponent implements OnInit {
    @Input() public text: string = "";

    constructor() { }

    ngOnInit() { }
}