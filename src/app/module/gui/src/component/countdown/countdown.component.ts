import { Component, OnInit, Input, OnChanges, OnDestroy, Output, EventEmitter } from '@angular/core';
import { differenceInSeconds, differenceInMilliseconds } from 'date-fns';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'kruCountdown',
    templateUrl: 'countdown.component.html'
})

export class CountdownComponent implements OnInit, OnDestroy, OnChanges {
    @Input('kruTime')
    time: any;
    @Output('kruOnClick')
    protected clickEvent: EventEmitter<any>;
    @Output('kruOnEnd')
    protected endEvent: EventEmitter<any>;

    public countdown: any;
    public text: string = "";
    private interval: any = null;

    constructor(
        private translateService: TranslateService
    ) {
        this.clickEvent = new EventEmitter();
        this.endEvent = new EventEmitter();
        this.countdown = {};
        this.resetCountdown();
    }

    ngOnInit() {
        this.updateCountdown();
        this.interval = setInterval(() => {
            this.updateCountdown();   
        }, 1000);
    }

    ngOnDestroy(): void {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    ngOnChanges(): void {
        this.updateCountdown();
    }

    private updateCountdown(): void {
        let now: any = new Date();
        let reminder: number = differenceInSeconds(this.time, now);
        if (differenceInMilliseconds(this.time, now) < 0) {
            this.resetCountdown();
            this.text = this.getCountdownText();
            return;
        } else if (reminder === 0) {
            this.endEvent.emit();
        }
        this.countdown.total = reminder;
        this.countdown.hours = Math.floor(reminder / 3600);
        reminder = reminder % 3600;
        this.countdown.minutes = Math.floor(reminder / 60);
        reminder = reminder % 60;
        this.countdown.seconds = reminder;

        this.text = this.getCountdownText();
    }

    private resetCountdown(): void {
        this.countdown.hours = 0;
        this.countdown.minutes = 0;
        this.countdown.seconds = 0;
        this.countdown.total = 0;
    }

    public getCountdownText(): string {
        if (this.countdown.total > 0) {
            if (this.countdown.hours > 0) {
                return this.translateService.instant('countdown.minutes', {value: "60+"});
            } else if (this.countdown.minutes > 0) {
                return this.translateService.instant('countdown.minutes', {value: this.countdown.minutes});
            } else {
                return this.translateService.instant('countdown.seconds', {value: this.countdown.seconds});
            }
        }

        return "";
   }

   public onClick(event: any): void {
        this.clickEvent.emit();
   }
}