import { Component, Input, HostBinding } from "@angular/core";
import { animations } from "src/app/module/gui/src/animation/animation";
import { ObservableList } from '@wildebeest/observe-changes';
import { ToastEntity } from 'src/app/module/toast';

@Component({
    selector: 'kruToasts',
    templateUrl: 'toasts.component.html',
    styleUrls: ['toast.scss'],
    animations: [animations]
})
export class ToastsComponent {
    @Input('toasts')
    public toasts: ObservableList<ToastEntity>;

    @HostBinding('class.toast-index')
    public hasIndexClass: boolean = true;
}