import { Component, Input } from '@angular/core';
import { GuestEntity } from 'src/app/module/guest';

@Component({
    selector: 'kruUserInitials',
    templateUrl: 'initials.component.html'
})

export class InitialsComponent {
    @Input('kruUser')
    public user: GuestEntity;

    constructor() {}
}