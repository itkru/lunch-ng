import { Component, Output, EventEmitter, Input, OnInit, OnDestroy } from "@angular/core";
import { ComponentModel } from '../../model/component.model';
import { ObservableProperty, Closable, PropertyChange } from '@wildebeest/observe-changes';

@Component({
    selector: 'kruNavigation',
    templateUrl: 'navigation.component.html'
})
export class NavigationComponent implements OnInit, OnDestroy {
    @Input('navigationModel') public navigationModel: ComponentModel;

    @Output('onCreate') public createEmitter: EventEmitter<any>;
    @Output('onCreateClose') public createCloseEmitter: EventEmitter<any>;
    @Output('onProfile') public profileEmitter: EventEmitter<any>;
    @Output('onProfileClose') public profileCloseEmitter: EventEmitter<any>;
    @Output('onShare') shareEmitter: EventEmitter<any>;

    public createActiveProperty: boolean = false;
    public profileActiveProperty: boolean = false;
    private closables: Closable[] = [];

    constructor() {
        this.createEmitter = new EventEmitter();
        this.createCloseEmitter = new EventEmitter();
        this.profileEmitter = new EventEmitter();
        this.profileCloseEmitter = new EventEmitter()
        this.shareEmitter = new EventEmitter();
    }

    ngOnInit(): void {
        this.closables.push(
            this.navigationModel.attributes().property('active')
                .addListenerAndCall(
                    this.onActiveChange.bind(this)
                )
        );
    }

    ngOnDestroy(): void {
        for (let closable of this.closables) {
            closable.close();
        }
    }

    private onActiveChange(change: PropertyChange<string>): void {
        this.createActiveProperty = change.next() == 'create';
        this.profileActiveProperty = change.next() == 'profile';
    }
}