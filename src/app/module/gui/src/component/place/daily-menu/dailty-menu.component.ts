import { Input, Component } from "@angular/core";

@Component({
    selector: 'kruDailyMenu',
    templateUrl: 'daily-menu.component.html'
})
export class DailyMenuComponent {
    @Input() public menu: any;
}