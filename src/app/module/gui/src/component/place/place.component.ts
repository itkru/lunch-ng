import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from "@angular/core";
import { PlaceEntity } from "src/app/module/place";
import { EventDispatcher } from "src/app/module/dispatcher";
import { Event } from "src/app/lib/event/event";

@Component({
    selector: 'kruPlace',
    templateUrl: 'place.component.html'
})
export class PlaceComponent implements OnInit, OnChanges {
    @Input()
    public place: PlaceEntity;
    @Input()
    public loading: boolean;
    @Output('onClose')
    public onCloseEmitter: EventEmitter<any>;

    constructor(
        private dispatcher: EventDispatcher
    ) {
        this.onCloseEmitter = new EventEmitter();
    }


    ngOnInit(): void {

    }

    ngOnChanges(): void {

    }

    public closePlace(): void {
        this.onCloseEmitter.emit();
    }
}