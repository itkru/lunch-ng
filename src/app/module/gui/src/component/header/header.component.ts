import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'kruHeader',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.scss']
})

export class HeaderComponent implements OnInit {
    @Input('kruTitle') public title: string = "";
    @Input('loading') public isLoading: boolean = false;
    @Input('timelineText') public timelineText: string = "";
    @Output('onBack') protected onBack: EventEmitter<any>;
    public loading: boolean = false;

    constructor() {
        this.onBack = new EventEmitter();
    }

    public ngOnInit() {}

    public back(): void {
        this.onBack.emit();
    }

    public isBackVisible(): boolean {
        return this.onBack.observers.length > 0;
    }
}