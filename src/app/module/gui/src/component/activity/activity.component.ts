import { Component, Input, HostListener, ElementRef, Output, EventEmitter, OnInit, OnDestroy, HostBinding } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TextFormaterService } from 'src/app/module/template';
import { PlaceEntity } from 'src/app/module/place';
import { ActivityEntity } from 'src/app/module/activity';
import { GuestEntity } from 'src/app/module/guest';
import { GuiView } from '../../view/gui.view';
import { animations } from '../../animation/animation';
import { ThrottleTask } from 'src/app/lib/task/src/throttle.task';
import { ComponentModelRepository } from '../../service/component-model.repository';
import { ComponentModel } from '../../model/component.model';
import { PropertyChange } from '@wildebeest/observe-changes';
import { Event } from 'src/app/lib/event/event';

@Component({
    selector: 'kruActivity',
    templateUrl: 'activity.component.html',
    animations: [animations]
})

export class ActivityComponent extends GuiView implements OnInit, OnDestroy {
    @Input('kruActivity')
    public activity: ActivityEntity;
    public model: ComponentModel;
    private maxPeople: number = 6;
    private scrollThrottleTask: ThrottleTask;

    @Output('onMenuClick')
    public menuClickEmitter: EventEmitter<PlaceEntity>;
    @Output('onJoin')
    public joinEmitter: EventEmitter<ActivityEntity>;
    @Output('onLeave')
    public leaveEmitter: EventEmitter<ActivityEntity>;
    @HostBinding('class')
    public class: string = "card";

    constructor(
        protected route: ActivatedRoute,
        private element: ElementRef,
        private textFormaterService: TextFormaterService,
        protected router: Router,
        private componentModelRepository: ComponentModelRepository
    ) {
        super();
        this.menuClickEmitter = new EventEmitter();
        this.joinEmitter = new EventEmitter();
        this.leaveEmitter = new EventEmitter();
        this.scrollThrottleTask = new ThrottleTask(
            this.onScroll.bind(this),
            10
        );
    }

    ngOnInit() {
        this.model = this.componentModelRepository.get("activity:" + this.activity.getId());

        this.closables.push(
            this.model.attributes().property<boolean>('selected').addListenerAndCall((change: PropertyChange<boolean>) => {
                if (change.next()) {
                    this.class = "card card--inverse";
                } else {
                    this.class = "card";
                }
            })
        );

        this.subscriptions.push(
            this.model.dispatcher()
                .observe("scrollTo")
                .subscribe((event: Event) => {
                    if (!event) {
                        return;
                    }
                    this.scrollTo();
                })
        );

        this.scrollThrottleTask.run();
    }

    private isInViewport(): boolean {
        let rect: any = this.element.nativeElement.getBoundingClientRect();
        let windowHeight: number = window.innerHeight || document.documentElement.clientHeight;
        let headerHeight: number = 100;
        return (rect.height + rect.top - headerHeight > 0 && rect.top < windowHeight - rect.height);
    }

    @HostListener('window:scroll', ['$event'])
    public throttleScroll(event: any): void {
        this.scrollThrottleTask.run();
    }

    public onScroll(event: any): void {
        if (!this.model.attributes().get('new')) {
            return;
        }
        if (!this.isInViewport()) {
            return;
        }
        this.model.attributes().disable('new');
    }

    public getPlusUsers(): number {
        return this.activity.getUsers().length - this.maxPeople;
    }

    public getPreviewUsers(): Array<GuestEntity> {
        return this.activity.getUsers().slice(0, this.maxPeople);
    }

    public trackPreviewUsers(index: number, user: GuestEntity): any {
        return user.getId();
    }

    public join(): void {
        this.joinEmitter.emit(this.activity);
    }

    public leave(): void {
        this.leaveEmitter.emit(this.activity);
    }

    public isSelected(): boolean {
        return this.model.attributes().get('selected');
    }

    public isPeopleVisible(): boolean {
        return this.model.attributes().get('peopleVisible');
    }

    public togglePeople(): void {
        this.model.attributes().toggle('peopleVisible')
    }

    public scrollTo(): void {
        this.element.nativeElement.scrollIntoView();
        let rect: any = this.element.nativeElement.getBoundingClientRect();
        if (rect.top < 100) {
            document.scrollingElement.scrollTop += rect.top - 100;
        }
    }

    public getTextComponent(): any[] {
        let text: string = this.activity.getInfo();
        let components: any[] = [this.textFormaterService.makePlain(text)];
        components = this.textFormaterService.detectLinks(components);
        return components;
    }

    public showPlace(place: PlaceEntity): void {
        this.menuClickEmitter.emit(place);
    }
}