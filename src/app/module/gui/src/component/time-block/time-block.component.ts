import { Component, Input, ElementRef, HostListener, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { ActivityEntity } from 'src/app/module/activity';
import { PlaceEntity } from 'src/app/module/place';
import { format } from 'date-fns';
import { TimeBlockEntity } from '../../model/time-block.entity';

@Component({
    selector: 'kruTimeBlock',
    templateUrl: 'time-block.component.html'
})

export class TimeBlockComponent implements AfterViewInit {
    @Input('kruBlocks')
    public block: TimeBlockEntity;
    @Input('hideTimeline')
    public hideTimeline: boolean = false;

    public stickyContainerId: string;
    @Output('onViewport')
    protected viewportEmitter: EventEmitter<any>;
    @Output('onMenuClick')
    public menuClickEmitter: EventEmitter<PlaceEntity>;
    @Output('onJoin')
    public joinEmitter: EventEmitter<ActivityEntity>;
    @Output('onLeave')
    public leaveEmitter: EventEmitter<ActivityEntity>;
    @Output('onRecommend')
    public recommendEmitter: EventEmitter<any>;


    constructor(
        private element: ElementRef
    ) {
        this.viewportEmitter = new EventEmitter();
        this.menuClickEmitter = new EventEmitter();
        this.joinEmitter = new EventEmitter();
        this.leaveEmitter = new EventEmitter();
        this.recommendEmitter = new EventEmitter();
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.onScroll({
                target: {
                    scrollingElement: this.element.nativeElement
                }
            });
        });
    }

   public trackActivities(index: number, activity: ActivityEntity): string {
        return activity.getId();
   }

   @HostListener('window:scroll', ['$event']) public onScroll(event: any): void {
        let scrollPosition: number = event.target.scrollingElement.scrollTop;
        if (this.isScrollColision(scrollPosition)) {
            this.viewportEmitter.emit(format(this.block.getTime(), "HH:mm"));
        }
    }

    private isScrollColision(scrollPosition: number): boolean {
        let height: number = this.element.nativeElement.offsetHeight;
        return scrollPosition >= this.element.nativeElement.offsetTop && scrollPosition < this.element.nativeElement.offsetTop + height;

    }

    public recommend(place: PlaceEntity): void {
        this.recommendEmitter.emit({
            startAt: this.block.getTime(),
            place: place,
            text: ""
        });

    }
}