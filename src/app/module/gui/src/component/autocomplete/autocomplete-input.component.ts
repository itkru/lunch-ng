import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";
import { OptionModel } from "../../model/option.model";
import { DebounceTask } from "src/app/lib/task/src/debounce.task";
import { ObservableList, ObservableProperty, SimpleObservableProperty, Closable, ListChange } from '@wildebeest/observe-changes';
import { ComponentModel } from '../../model/component.model';

@Component({
    selector: 'kruAutocompleteInput',
    templateUrl: 'autocomplete-input.component.html',
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: AutocompleteInputComponent, multi: true}
    ],
})
export class AutocompleteInputComponent implements ControlValueAccessor, OnInit, OnDestroy {
    @Output('onInputChange')
    public changeEmitter: EventEmitter<string>;
    @Input('autocompleteOptions')
    public options: ObservableList<OptionModel<any>>;
    @Input('autocompletePosition')
    public position: string = "bottom";
    @Input('autocompletePlaceholder')
    public placeholder: string = "";
    @Input('componentModel')
    public model: ComponentModel;

    public activeOption: any = null;
    public inputValue: any = "";
    public optionsVisible: boolean = false;
    public changeDebounceTask: DebounceTask;
    private innerValue: any = {name: ""};
    private focusProperty: ObservableProperty<boolean>;

    private changed: any[] = [];
    private touched: any[] = [];
    private closables: Closable[] = [];

    constructor() {
        this.changeEmitter = new EventEmitter();
        this.focusProperty = new SimpleObservableProperty(false);
        this.changeDebounceTask = new DebounceTask(() => {
            this.changeEmitter.emit(this.inputValue);
        }, 320);
    }

    get value(): any {
        return this.innerValue;
    }

    set value(value: any) {
        if (this.innerValue === value) {
            return;
        }
        this.innerValue = value;
        if (this.innerValue) {
            this.inputValue = this.innerValue.name;
        } else {
            this.inputValue = "";
        }
        
        for (let i: number = 0; i < this.changed.length; i++) {
            this.changed[i](this.innerValue);
        }
    }

    ngOnInit(): void {
        this.closables.push(
            this.options.addListenerAndCall(
                this.onOptionsChange.bind(this)
            )
        );
        this.closables.push(
            this.focusProperty.addListenerAndCall(() => {
                this.decideVisibility();
            })
        );
    }

    ngOnDestroy(): void {
        for (let closable of this.closables) {
            closable.close();
        }
    }

    private onOptionsChange(change: ListChange<OptionModel<any>>): void {
        this.decideVisibility();
        if (this.options.count() > 0) {
            this.activateItem(this.options.get(0));
        }
    }

    private decideVisibility(): void {
        this.optionsVisible = this.focusProperty.get() === true && !this.options.isEmpty();
    }

    writeValue(value: any): void {
        this.value = value;
    }

    registerOnChange(callback: any): void {
        this.changed.push(callback);
    }

    registerOnTouched(callback: any): void {
        this.touched.push(callback);
    }

    public touch(): void {
        for (let i: number = 0; i < this.touched.length; i++) {
            this.touched[i]();
        }
    }

    public onInputChange(value: string): void {
        this.writeValue(value);
        if (value === "") {
            this.options.setAll([]);
            return;
        }
        this.changeDebounceTask.run();
    }

    public onFocus(): void {
        this.focusProperty.set(true);
    }

    public onBlur(): void {
        this.focusProperty.set(false);
    }

    public activateItem(item: OptionModel<any>): void {
        if (this.activeOption) {
            this.deactivateItem(this.activeOption);
        }
        this.activeOption = item;
        if (!this.activeOption) {
            return;
        }
        this.activeOption.enable('active');
    }

    public deactivateItem(item: OptionModel<any>): void {
        item.disable('active');
    }

    private activateNext(): void {
        let index: number = this.options.all().indexOf(this.activeOption);
        if (index == -1) {
            return;
        }
        if (this.options.count() == index + 1) {
            this.activateItem(this.options.get(0));
        } else {
            this.activateItem(this.options.get(index + 1));
        }
    }

    private activatePrevious(): void {
        let index: number = this.options.all().indexOf(this.activeOption);
        if (index == -1) {
            return;
        }
        if (index < 1) {
            this.activateItem(this.options.get(this.options.count() - 1));
        } else {
            this.activateItem(this.options.get(index - 1));
        }
    }

    public onKeydown(event: any): void {
        switch (event.keyCode) {
            // Arrow down
            case 40:
                event.preventDefault();
                if (this.options.count() == 0) {
                    break
                }
                if (this.optionsVisible) {
                    this.activateNext();
                } else {
                    this.decideVisibility();
                }
                break;
            // Arrow up
            case 38:
                this.activatePrevious();
                event.preventDefault();
                break;
            // Tab
            case 9:
                if (this.optionsVisible) {
                    event.preventDefault();
                    this.selectValue(this.activeOption.getValue());
                }
                break;
            // Enter
            case 13:
                if (!event.shiftKey && this.optionsVisible) {
                    event.preventDefault();
                    this.selectValue(this.activeOption.getValue());
                }
                break;
            // ESC
            case 27:
                this.optionsVisible = false;
                event.preventDefault();
                break;
        }
    }

    public selectValue(place: any): void {
        this.writeValue(place);
        this.inputValue = place.name;
        this.optionsVisible = false;
    }
}