import { trigger, state, style, animate, transition, } from '@angular/animations';

export const animations = trigger('animate', [
    state('*', style({
        transform: 'scale(1)',
        opacity: 1
    })),
    state('void', style({
        transform: 'scale(0.8)',
        opacity: 0
    })),
    transition('* => void', animate('200ms ease')),
    transition('void => *', animate('200ms ease'))
]);