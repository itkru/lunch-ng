import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { HomeView } from "./view/home/home.view";
import { RoomView } from "./view/room/room.view";
import { PlaceComponent } from "./component/place/place.component";
import { DailyMenuComponent } from "./component/place/daily-menu/dailty-menu.component";
import { HeaderComponent } from "./component/header/header.component";
import { WelcomeView } from "./view/welcome/welcome.view";
import { RoomShellView } from "./view/room-shell/room-shell.view";
import { RouterModule } from "@angular/router";
import { NavigationComponent } from "./component/navigation/navigation.component";
import { TooltipDirective } from "./directive/tooltip.directive";
import { StepTimeView } from "./view/step-time/step-time.view";
import { NgxMaterialTimepickerModule } from "ngx-material-timepicker";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { StepPlaceView } from "./view/step-place/step-place.view";
import { StepInfoView } from "./view/step-info/step-info.view";
import { ProfileView } from "./view/profile/profile.view";
import { FocusClassDirective } from "./directive/focus-class.directive";
import { AutocompleteInputComponent } from "./component/autocomplete/autocomplete-input.component";
import { Autosize } from "./directive/autosize.directive";
import { AutofocusDirective } from "./directive/autofocus.directive";
import { TimeBlockComponent } from "./component/time-block/time-block.component";
import { ActivityComponent } from "./component/activity/activity.component";
import { TemplateModule } from "../../template";
import { InitialsComponent } from "./component/initials/initials.component";
import { CountdownComponent } from "./component/countdown/countdown.component";
import { ToastsComponent } from "./component/toasts/toasts.component";
import { ToastComponent } from "./component/toast/toast.component";
import { AppView } from "./view/app/app.view";
import { ClipboardModule } from "ngx-clipboard";
import { TranslateModule } from '@ngx-translate/core';
import { ComponentModelRepository } from './service/component-model.repository';

@NgModule({
    imports: [CommonModule, FormsModule, RouterModule, NgxMaterialTimepickerModule, BrowserAnimationsModule, FormsModule, ReactiveFormsModule, TemplateModule, ClipboardModule, TranslateModule],
    exports: [HomeView, RoomShellView, RoomView, StepTimeView, StepPlaceView, StepInfoView, ProfileView, AppView],
    declarations: [
        HomeView, WelcomeView, RoomShellView, RoomView, StepTimeView, StepPlaceView, StepInfoView, ProfileView, AppView,
        ToastsComponent, ToastComponent, PlaceComponent, DailyMenuComponent, HeaderComponent, NavigationComponent, AutocompleteInputComponent, TimeBlockComponent, ActivityComponent, InitialsComponent, CountdownComponent,
        TooltipDirective, FocusClassDirective, Autosize, AutofocusDirective
    ],
    providers: [ComponentModelRepository]
})
export class GuiModule {

}