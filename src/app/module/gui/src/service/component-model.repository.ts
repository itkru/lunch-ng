import { Injectable } from '@angular/core';
import { ComponentModel } from '../model/component.model';

@Injectable()
export class ComponentModelRepository {
    private values: Map<string, ComponentModel>;

    public constructor() {
        this.values = new Map();
    }

    public get(key: string): ComponentModel {
        if (!this.values.has(key)) {
            this.values.set(key, new ComponentModel());
        }
        return this.values.get(key);
    }

    public remove(key: string): void {
        this.values.delete(key);
    }
}