import { Input, HostBinding } from "@angular/core";

export class ClassDirective {
    private classList: string[] = [];
    @Input('class')
    @HostBinding('class')
    get classString(): string {
        return this.classList.join(" ");
    }
    set classString(classes: string) {
        this.classList = classes.split(" ");
    }
    
    public addClass(className: string): void {
        if (this.classList.indexOf(className) > -1) {
            return;
        }
        this.classList.push(className);
    }

    public removeClass(className: string): void {
        while (this.classList.indexOf(className) > -1) {
            this.classList.splice(this.classList.indexOf(className), 1);
        }
    }
}