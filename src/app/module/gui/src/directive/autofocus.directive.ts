import { Directive, OnInit, AfterViewInit, ElementRef } from '@angular/core';

@Directive({
    selector: '[kruAutofocus]'
})
export class AutofocusDirective implements OnInit, AfterViewInit {
    constructor(private elementRef: ElementRef) { }

    ngOnInit(): void {
        
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.elementRef.nativeElement.focus();
        });
    }
}
