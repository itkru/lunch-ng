import { ElementRef, HostListener, Directive, Inject, OnInit, Input } from '@angular/core';

@Directive({
    selector: 'textarea[kruAutosize]'
})

export class Autosize implements OnInit {
  @Input('kruAutosize') private baseHeight = "auto";
 @HostListener('input',['$event.target'])
  onInput(textArea: HTMLTextAreaElement): void {
    this.adjust();
  }
  constructor(public element: ElementRef){
    
  }
  ngOnInit(): void {
    this.adjust();
  }
  ngAfterContentChecked(): void {}
  
  adjust(): void{
    let scrollTop: number = window.scrollY;
    let scrollLeft: number = window.scrollX
    this.element.nativeElement.style.height = this.baseHeight;
    this.element.nativeElement.style.height = (this.element.nativeElement.scrollHeight + 2) + "px";
    window.scrollTo(scrollLeft, scrollTop);
  }
}
