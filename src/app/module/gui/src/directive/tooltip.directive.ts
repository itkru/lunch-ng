import { Directive, Input, HostBinding, ElementRef, HostListener, OnInit, OnChanges } from "@angular/core";
import { ClassDirective } from "./class-directive";

@Directive({
    selector: '[kruTooltip]'
})
export class TooltipDirective extends ClassDirective implements OnChanges {
    @Input('kruTooltip')
    @HostBinding('attr.data-tooltip')
    public tooltipText: string = "";

    @Input('preferVertical')
    public preferVertical: string = "";

    @Input('tooltipPosition')
    public defaultPosition: string = "top";

    @Input('tooltipForce')
    public forceShowTooltip: boolean = false;

    @Input('tooltipDisabled')
    public disabled: boolean = false;

    private currentClass: string = "";
    private constrains: number = 50;
    private positionClassMapping: any = {
        'none-default': 'tooltip--right',
        'none-left': 'tooltip--left',
        'none-right': 'tooltip--right',
        'default-left': 'tooltip--left',
        'default-right': 'tooltip--right',
        'default-none': 'tooltip--top',
        'top-left': 'tooltip--top-left',
        'top-right': 'tooltip--top-right',
        'top-default': 'tooltip--top',
        'top-none': 'tooltip--top',
        'bottom-left': 'tooltip--bottom-left',
        'bottom-right': 'tooltip--bottom-right',
        'bottom-default': 'tooltip--bottom',
        'bottom-none': 'tooltip--bottom',
        "top": "tooltip--top",
        "right": "tooltip--right",
        "left": "tooltip--left",
        "bottom": "tooltip--bottom",
    };

    constructor(
        private element: ElementRef
    ) {
        super();
    }

    ngOnChanges(): void {
        if (this.forceShowTooltip) {
            this.show();
            this.addClass('tooltip--show');
        } else {
            this.removeClass('tooltip--show');
            this.removeClass('tooltip');
        }
    }

    private getCurrentVerticalPosition(): string {
        let elementRect: any = this.element.nativeElement.getBoundingClientRect();
        if (elementRect.top <= this.constrains && elementRect.bottom >= window.innerHeight - this.constrains) {
            return "none";
        } else if (elementRect.top <= this.constrains) {
            return "bottom";
        } else if (elementRect.bottom >= window.innerHeight - this.constrains) {
            return "top";
        }
        if (this.preferVertical.length > 0) {
            return this.preferVertical;
        }
        return "default";
    }

    private getCurrentHorizontalPosition(): string {
        let elementRect: any = this.element.nativeElement.getBoundingClientRect();
        if (elementRect.left <= this.constrains && elementRect.right >= window.innerWidth - this.constrains) {
            return "none";
        } else if (elementRect.left <= this.constrains) {
            return "right";
        } else if (elementRect.right >= window.innerWidth - this.constrains) {
            return "left";
        }
        return "default";
    }


    private getCurrentPosition(): string {
        let verticalPosition: string = this.getCurrentVerticalPosition();
        let horizontalPosition: string = this.getCurrentHorizontalPosition();

        return verticalPosition + "-" + horizontalPosition;
    }

    private getCurrentClass(): string {
        let position: string = this.getCurrentPosition();
        if (position == "default-default") {
            position = this.defaultPosition;
        }
        return this.positionClassMapping[position];
    }

    @HostListener('mouseenter')
    public show(): void {
        if (this.tooltipText.length === 0 || this.disabled) {
            return;
        }
        this.currentClass = this.getCurrentClass();
        this.addClass(this.currentClass);
        this.addClass('tooltip');
    }

    @HostListener('mouseleave')
    public hide(): void {
        if (this.forceShowTooltip) {
            return;
        }
        this.removeClass(this.currentClass);
        this.removeClass('tooltip');
    }
}