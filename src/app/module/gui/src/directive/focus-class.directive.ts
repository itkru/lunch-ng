import { Directive, Input, OnInit, OnDestroy } from "@angular/core";
import { ClassDirective } from "./class-directive";

@Directive({
    selector: '[kruFocusClass]'
})
export class FocusClassDirective extends ClassDirective implements OnInit, OnDestroy {
    @Input('kruFocusClass')
    public focusClass: string;
    @Input('bindFocusElement')
    public focusElement: any;

    ngOnInit(): void {
        this.focusElement.addEventListener('focus', this.onFocus.bind(this));
        this.focusElement.addEventListener('blur', this.onBlur.bind(this));
    }

    ngOnDestroy(): void {
        this.focusElement.removeEventListener('focus', this.onFocus.bind(this));
        this.focusElement.removeEventListener('blur', this.onBlur.bind(this));
    }

    private onFocus(event: any): void {
        this.addClass(this.focusClass);
    }

    private onBlur(event: any): void {
        this.removeClass(this.focusClass);
    }
}