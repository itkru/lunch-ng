import { Component } from '@angular/core';
import { ToastStore, ToastEntity } from 'src/app/module/toast';
import { ObservableList } from '@wildebeest/observe-changes';

@Component({
    selector: 'kruApp',
    templateUrl: './app.view.html'
})
export class AppView {
    public toasts: ObservableList<ToastEntity>;

    constructor (
        toastStore: ToastStore
    ) {
        this.toasts = toastStore.getList();
    }
}
