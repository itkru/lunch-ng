declare var Cookies: any;

import { Component, OnInit } from '@angular/core';
import { EventDispatcher } from 'src/app/module/dispatcher';
import { Event } from 'src/app/lib/event/event';
import { GuestEntity } from 'src/app/module/guest';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { ObservableProperty } from '@wildebeest/observe-changes';
import { ComponentModelRepository } from '../../service/component-model.repository';
import { ComponentModel } from '../../model/component.model';

@Component({
    selector: 'kruWelcome',
    templateUrl: 'welcome.view.html'
})

export class WelcomeView implements OnInit {
    public userProperty: ObservableProperty<GuestEntity>;
    public model: ComponentModel;
    public name: string;

    constructor(
        protected dispatcher: EventDispatcher,
        private route: ActivatedRoute,
        private titleService: Title,
        private translateService: TranslateService,
        private modelRepository: ComponentModelRepository
    ) {
        this.model = modelRepository.get("auth");
    }

    public ngOnInit(): void {
        Cookies.set('show_notification_tooltip', true);

        this.titleService.setTitle(
            this.translateService.instant('welcome.meta.title')
        );

        let data: any = this.route.snapshot.data.auth;
        this.userProperty = data.user;
    }

    public create(event: any): void {
        this.dispatcher.dispatch(
            new Event("auth.signup", this.name)
        );
    }
}