import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { RoomEntity } from 'src/app/module/room';
import { PlaceEntity } from 'src/app/module/place';
import { ActivityEntity } from 'src/app/module/activity';
import { EventDispatcher } from 'src/app/module/dispatcher';
import { Event } from 'src/app/lib/event/event';
import { format } from 'date-fns';
import { GuiView } from '../gui.view';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { ObservableList, ObservableProperty, PropertyChange } from '@wildebeest/observe-changes';
import { TimeBlockEntity } from '../../model/time-block.entity';
import { ComponentModelRepository } from '../../service/component-model.repository';
import { ComponentModel } from '../../model/component.model';

@Component({
    selector: 'kruRoom',
    templateUrl: 'room.view.html',
})
export class RoomView extends GuiView implements OnInit {
    public model: ComponentModel;
    protected placeProperty: ObservableProperty<PlaceEntity>;
    public blocks: ObservableList<TimeBlockEntity>;
    public notificationModel: ComponentModel;
    public place: PlaceEntity;
    public roomProperty: ObservableProperty<RoomEntity>;
    public room: RoomEntity;
    public dailyMenu: any;
    public activeTime: string;
    public selectedActivityProperty: ObservableProperty<ActivityEntity>;
    public selectedActivity: ActivityEntity;
    private motivations: ObservableList<string>;
    public motivationText: string = "";
    public countdownTime: Date;
    public countdownTooltip: string = "";

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private dispatcher: EventDispatcher,
        private titleService: Title,
        private translateService: TranslateService,
        private componentModelRepository: ComponentModelRepository
    ) {
        super();
        this.model = this.componentModelRepository.get("room:active");
        this.notificationModel = this.componentModelRepository.get("browser-notification");
    }

    ngOnInit() {
        let data: any = this.route.snapshot.data.view;

        this.blocks = data.blocks;
        this.motivations = data.greetings;
        this.roomProperty = data.room;
        this.placeProperty = data.place;
        this.selectedActivityProperty = data.selectedActivity;

        this.closables.push(
            this.motivations.addListenerAndCall(
                this.onMotivationsChange.bind(this)
            )
        );

        this.closables.push(
            this.roomProperty.addListenerAndCall(
                this.onRoomChange.bind(this)
            )
        );

        this.closables.push(
            this.selectedActivityProperty.addListenerAndCall(
                this.onSelectedActivityChange.bind(this)
            )
        );

        this.closables.push(
            this.placeProperty.addListenerAndCall(
                this.onPlaceChane.bind(this)
            )
        );
    }

    private onRoomChange(change: PropertyChange<RoomEntity>): void {
        this.room = change.next();
        if (this.room === null) {
            return;
        }
        this.titleService.setTitle(
            this.translateService.instant('room.meta.title', {value: this.room.getName()})
        );
    }

    private onSelectedActivityChange(change: PropertyChange<ActivityEntity>): void {
        this.selectedActivity = change.next();
        if (this.selectedActivity) {
            this.countdownTime = this.selectedActivity.getStartAt();
            if (this.countdownTime < new Date()) {
                this.countdownTooltip = this.translateService.instant('countdown.finnish-tooltip');
            } else {
                this.countdownTooltip = this.translateService.instant('countdown.tooltip', {value: format(this.countdownTime, "HH:mm")});
            }
            
        } else {
            this.countdownTime = null;
        }
    }

    private onPlaceChane(change: PropertyChange<PlaceEntity>): void {
        this.place = change.next();
        this.dailyMenu = null;
        if (this.place !== null) {
            this.dailyMenu = this.place.getDailyMenu();
        }
    }

    private onMotivationsChange(messages: string[]): void {
        this.motivationText = this.getRandomMotivationText();
    }

    public setHeaderText(text: string): void {
        this.activeTime = text;
    }

    public trackBlocks(index: number, block: TimeBlockEntity): string {
        return format(block.getTime(), 'HH:mm');
    }

    private getRandomMotivationText(): string {
        if (this.motivations.isEmpty()) {
            return "...";
        }
        let index: number = Math.floor(Math.random() * this.motivations.count());
        return this.motivations.get(index);
    }

    public scrollToActivity(): void {
        if (this.selectedActivityProperty.isEmpty()) {
            return;
        }

        let selectedActivityId: string = this.selectedActivityProperty.get().getId();
        this.componentModelRepository.get("activity:" + selectedActivityId).dispatcher().dispatch(
            new Event("scrollTo")
        );
    }

    public back(): void {
        this.dispatcher.dispatch(
            new Event("room.close")
        );
        this.router.navigate(["/"]);
    }

    public askBrowserNotificationPermission() {
        this.dispatcher.dispatch(
            new Event("notification.requestPermission")
        );
    }

    public showNotification(): void {
        this.dispatcher.dispatch(
            new Event("notification.show", {
                title: this.translateService.instant('room.activity.active.notification.title'),
                body: this.translateService.instant('room.activity.active.notification.body', {value: this.selectedActivity.getPlace().getName()})
            })
        );
    }

    public closePlace(): void {
        let params: ParamMap = this.route.parent.snapshot.paramMap;
        this.router.navigate([
            params.get('roomId')
        ]);
    }

    public openPlace(place: PlaceEntity): void {
        let params: ParamMap = this.route.parent.snapshot.paramMap;
        this.router.navigate([
            params.get('roomId'),
            'place',
            place.getId()
        ]);
    }

    public join(activity: ActivityEntity): void {
        this.dispatcher.dispatch(
            new Event("activity.join", activity)
        );
    }

    public leave(activity: ActivityEntity): void {
        this.dispatcher.dispatch(
            new Event("activity.leave", activity)
        );
    }

    public onRecommendClick(activity: any): void {
        this.dispatcher.dispatch(
            new Event(
                "activity.create", activity
            )
        );
    }

}