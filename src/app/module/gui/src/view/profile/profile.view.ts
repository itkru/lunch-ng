import { Component } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { EventDispatcher } from 'src/app/module/dispatcher';
import { Event } from 'src/app/lib/event/event';
import { GuiView } from '../gui.view';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { ComponentModelRepository } from '../../service/component-model.repository';
import { ComponentModel } from '../../model/component.model';

@Component({
    selector: 'kruProfile',
    templateUrl: 'profile.view.html'
})

export class ProfileView extends GuiView {
    public name: string;
    public model: ComponentModel;

    constructor(
        protected route: ActivatedRoute,
        protected router: Router,
        protected dispatcher: EventDispatcher,
        private titleService: Title,
        private translateService: TranslateService,
        private modelRepository: ComponentModelRepository
    ) {
        super();
        this.model = this.modelRepository.get("step:profile");
    }

    public ngOnInit(): void {
        this.titleService.setTitle(
            this.translateService.instant('profile.meta.title')
        );
        let data: any = this.route.snapshot.data.profile;
        this.name = data.user.get().getName();
    }

    public update(event: any): void {
        if (!this.name) {
            return;
        }
        this.dispatcher.dispatch(
            new Event("user.update", {name: this.name})
        );
    }

    public cancel(): void {
        let params: ParamMap = this.route.snapshot.parent.paramMap;
        this.router.navigate([
            params.get('roomId')
        ]);
    }

    public isFormValid(): boolean {
        return (this.name != "");
    }
}