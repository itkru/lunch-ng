import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { EventDispatcher } from 'src/app/module/dispatcher';
import { ActivityFactory } from 'src/app/module/activity';
import { Event } from 'src/app/lib/event/event';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { ComponentModel } from '../../model/component.model';
import { ComponentModelRepository } from '../../service/component-model.repository';
import { ObservableProperty, PropertyChange } from '@wildebeest/observe-changes';

@Component({
    selector: 'kruStepInfo',
    templateUrl: 'step-info.view.html'
})

export class StepInfoView {
    @ViewChild('form', {static: false})
    public formElement: any;

    public model: ComponentModel;
    public activityProperty: ObservableProperty<any>;
    public activity: any;

    constructor(
        protected route: ActivatedRoute,
        protected router: Router,
        protected dispatcher: EventDispatcher,
        protected activityFactory: ActivityFactory,
        private titleService: Title,
        private translateService: TranslateService,
        private modelRepository: ComponentModelRepository
    ) {
        this.model = this.modelRepository.get("room:active");
    }

    ngOnInit() {
        this.titleService.setTitle(
            this.translateService.instant('step.info.meta.title')
        );
        let data: any = this.route.snapshot.data.step;
        this.activityProperty = data.activityForm;

        this.activityProperty.addListenerAndCall(
            this.onActivityChange.bind(this)
        );
    }

    public back(): void {
        let params: ParamMap = this.route.snapshot.parent.paramMap;
        this.router.navigate([
            params.get('roomId'),
            'create',
            'place'
        ]);
    }

    public toStepTime(): void {
        let params: ParamMap = this.route.snapshot.parent.paramMap;
        this.router.navigate([
            params.get('roomId'),
            'create',
            'time'
        ]);
    }

    public submit(): void {
        this.dispatcher.dispatch(
            new Event("activity.create", this.activity)
        );
    }

    public onKeydown(event: any): boolean {
        if (event.keyCode == 13 && (this.activity.text.length == 0 || event.ctrlKey)) {
            this.formElement.ngSubmit.emit();
            return false;
        }
        return true;
    }

    private onActivityChange(change: PropertyChange<any>): void {
        this.activity = change.next();
    }
}