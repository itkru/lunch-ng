import { Component, ViewChild, ElementRef } from '@angular/core';
import { format } from 'date-fns';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { ObservableProperty, PropertyChange } from '@wildebeest/observe-changes';
import { GuiView } from '../gui.view';

@Component({
    selector: 'kruStepTime',
    templateUrl: 'step-time.view.html',
})

export class StepTimeView extends GuiView {
    @ViewChild('timeInput', {static: false})
    protected timeInput: ElementRef;
    public activity: any;
    public activityProperty: ObservableProperty<any>;
    public customTime: string;
    public hasCustomTime: boolean;
    public formatedTime: string;
    public radioHours: Array<string> = [];
    public radioMinutes: Array<string> = ['00', '15', '30', '45'];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private titleService: Title,
        private translateService: TranslateService
    ) {
        super();
        this.generateRadioHours();
    }

    ngOnInit() {
        this.titleService.setTitle(
            this.translateService.instant('step.time.meta.title')
        );
        let data: any = this.route.snapshot.data.step;
        this.activityProperty = data.activityForm;

        this.closables.push(
            this.activityProperty.addListenerAndCall(
                this.onActivityChange.bind(this)
            )
        );
    }

    private onActivityChange(change: PropertyChange<any>): void {
        this.activity = change.next();
        if (this.activity == null) {
            return;
        }

        this.onTimeChange(this.activity.startAt);
    }

    protected generateRadioHours(): void {
        let now: any = new Date();
        let baseHours = 10;
        if (now.getHours() > baseHours) {
            baseHours = now.getHours();
        }
        this.radioHours = [];
        for (let i = 0; i < 4; i++) {
            if (baseHours + i > 23) {
                continue;
            }
            this.radioHours.push((baseHours + i) + "");
        }
    }

    public cancel(): void {
        let params: ParamMap = this.route.snapshot.parent.paramMap;
        this.router.navigate([
            params.get('roomId')
        ]);
    }

    public submit(): void {
        let params: ParamMap = this.route.snapshot.parent.paramMap;
        this.router.navigate([
            params.get('roomId'),
            'create',
            'place'
        ]);
    }

    public setHour(hour: string): void {
        this.activity.startAt.setHours(parseInt(hour));
        this.onTimeChange(this.activity.startAt);
    }

    public setMinute(minute: string): void {
        this.activity.startAt.setMinutes(parseInt(minute));
        this.onTimeChange(this.activity.startAt);
    }

    private onTimeChange(startAt: Date): void {
        this.formatedTime = format(startAt, 'HH:mm');
        this.hasCustomTime = this.isCustomTime(startAt);
        if (this.hasCustomTime) {
            this.customTime = this.formatedTime;
        } else {
            this.customTime = "--:--";
        }
    }

    public openPicker(): void {
        this.timeInput.nativeElement.click();
    }

    private isCustomTime(startAt: Date): boolean {
        return this.radioHours.indexOf(format(startAt, "HH")) == -1 || this.radioMinutes.indexOf(format(startAt, "mm")) == -1;
    }

    public setTime(time: any): void {
        let parts: Array<string> = time.split(":");
        this.setHour(parts[0]);
        this.setMinute(parts[1])
    }
}