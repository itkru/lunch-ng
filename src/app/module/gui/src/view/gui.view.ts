import { OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { Closable } from '@wildebeest/observe-changes';

export class GuiView implements OnDestroy {
    protected subscriptions: Subscription[] = [];
    protected closables: Closable[] = [];

    ngOnDestroy(): void {
        for (let subscription of this.subscriptions) {
            subscription.unsubscribe();
        }
        for (let closable of this.closables) {
            closable.close();
        }
    }
}