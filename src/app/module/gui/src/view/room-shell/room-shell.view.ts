import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, RouterOutlet, ParamMap } from "@angular/router";
import { StepTimeView } from "../step-time/step-time.view";
import { StepPlaceView } from "../step-place/step-place.view";
import { StepInfoView } from "../step-info/step-info.view";
import { ProfileView } from "../profile/profile.view";
import { EventDispatcher } from "src/app/module/dispatcher";
import { Event } from "src/app/lib/event/event";
import { ClipboardService } from 'ngx-clipboard';
import { TranslateService } from '@ngx-translate/core';
import { ComponentModel } from '../../model/component.model';
import { ComponentModelRepository } from '../../service/component-model.repository';

@Component({
    selector: 'kruRoomShell',
    templateUrl: 'room-shell.view.html'
})
export class RoomShellView implements OnInit {
    public navigationModel: ComponentModel;
    @ViewChild('routerOutlet', {static: false})
    public routerOutlet: RouterOutlet;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private dispatcher: EventDispatcher,
        private clipboardService: ClipboardService,
        private translateService: TranslateService,
        private modelRepository: ComponentModelRepository
    ) {
        this.navigationModel = this.modelRepository.get('navigation');
    }

    ngOnInit(): void {}

    public onCreate(): void {
        let params: ParamMap = this.route.snapshot.paramMap;
        this.router.navigate([
            params.get('roomId'),
            'create',
            'time'
        ]);
    }

    public navigationClose(): void {
        let params: ParamMap = this.route.snapshot.paramMap;
        this.router.navigate([
            params.get('roomId')
        ]);
    }

    public onProfile(): void {
        let params: ParamMap = this.route.snapshot.paramMap;
        this.router.navigate([
            params.get('roomId'),
            'profile'
        ]);
    }

    public onActivateRoute(event: any): void {
        if (
            event instanceof StepTimeView ||
            event instanceof StepPlaceView ||
            event instanceof StepInfoView
        ) {
            this.navigationModel.attributes().set('active', 'create');
        } else if (event instanceof ProfileView) {
            this.navigationModel.attributes().set('active', 'profile');
        } else {
            this.navigationModel.attributes().set('active', '');
        }
    }

    public share(): void {
        let params: ParamMap = this.route.snapshot.paramMap;
        let roomUrl: string = window.location.origin + this.router.createUrlTree([params.get('roomId')]).toString();
        this.clipboardService.copyFromContent(roomUrl);
        this.dispatcher.dispatch(
            new Event(
                "toast.show",
                this.translateService.instant('share.toast')
            )
        );
    }
}