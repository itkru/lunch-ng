import { Component } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { OptionModel } from '../../model/option.model';
import { EventDispatcher } from 'src/app/module/dispatcher';
import { Event } from 'src/app/lib/event/event';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { GeoipEntity } from 'src/app/module/geoip';
import { GuiView } from '../gui.view';
import { ObservableProperty, PropertyChange, ObservableList } from '@wildebeest/observe-changes';
import { ComponentModel } from '../../model/component.model';
import { ComponentModelRepository } from '../../service/component-model.repository';
import { RoomEntity } from 'src/app/module/room';

@Component({
    selector: 'kruStepPlace',
    templateUrl: 'step-place.view.html'
})

export class StepPlaceView extends GuiView {
    private activityProperty: ObservableProperty<any>;
    private room: ObservableProperty<RoomEntity>
    public activity: any;
    public options: ObservableList<OptionModel<any>>;
    public stepValid: boolean;
    public geoipProperty: ObservableProperty<GeoipEntity>;
    public placeAutocompleteModel: ComponentModel;

    constructor(
        protected route: ActivatedRoute,
        protected router: Router,
        protected dispatcher: EventDispatcher,
        private titleService: Title,
        private translateService: TranslateService,
        private modelRepository: ComponentModelRepository
    ) {
        super();
        this.stepValid = false;
        this.placeAutocompleteModel = this.modelRepository.get('place:search');
    }

    ngOnInit() {
        this.titleService.setTitle(
            this.translateService.instant('step.place.meta.title')
        );
        let data: any = this.route.snapshot.data.step;
        
        this.activityProperty = data.activityForm;
        this.options = data.placeAutocomplete;
        this.geoipProperty = data.geoip;
        this.room = data.room;

        this.activityProperty.addListenerAndCall(
            this.onActivityChange.bind(this)
        );
    }

    public onPlaceChange(place: any) {
        if (typeof place === "string") {
            place = {
                id: null,
                name: place
            };
        }
        this.activity.place = place;
        this.stepValid = this.activity.place && this.activity.place.name !== "";
    }

    public back(): void {
        let params: ParamMap = this.route.snapshot.parent.paramMap;
        this.router.navigate([
            params.get("roomId"),
            'create',
            'time'
        ]);
    }

    public submit(): void {
        if (this.stepValid === false) {
            return;
        }
        let params: ParamMap = this.route.snapshot.parent.paramMap;
        this.router.navigate([
            params.get("roomId"),
            'create',
            'info'
        ]);
    }

    public loadPlaceOptions(placeName: string): void {
        let roomId: string = ""
        if (this.room.get()) {
            roomId = this.room.get().getId()
        }
        this.dispatcher.dispatch(
            new Event('place.search', {
                placeName: placeName,
                roomId: roomId
            })
        );
    }

    public requestGeolocation(): void {
        this.dispatcher.dispatch(
            new Event("gps.request")
        );
    }

    private onActivityChange(change: PropertyChange<any>): void {
        this.activity = change.next();
    }
}