import { Component, OnInit, OnDestroy, HostBinding } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EventDispatcher } from 'src/app/module/dispatcher';
import { RandomCycleTypewriter } from 'src/app/lib/typewriter/randomcycletypewriter.object';
import { environment } from 'src/environments/environment';
import { Event } from 'src/app/lib/event/event';
import { GuiView } from '../gui.view';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';
import { ObservableProperty, PropertyChange } from '@wildebeest/observe-changes';

@Component({
    selector: 'kruHome',
    templateUrl: 'home.view.html',
    styleUrls: ['home.view.scss']
})

export class HomeView extends GuiView implements OnInit, OnDestroy {
    @HostBinding('class.pane') paneClass: boolean = true;
    public name: string;
    public connectRoomName: string = "";
    public showParselinkTooltip: boolean = false;;

    protected typewriter: RandomCycleTypewriter;
    public placeholder: string = "";
    public languageProperty: ObservableProperty<string>;
    public language: string = "";

    constructor (
        private router: Router,
        private dispatcher: EventDispatcher,
        private route: ActivatedRoute,
        private titleService: Title,
        private translateService: TranslateService
    ) {
        super();
        this.typewriter = new RandomCycleTypewriter(
            translateService.stream('home.codeExamples'), {
            charsPerSecond: 5
        });
        this.typewriter.property().addListenerAndCall((change) => {
            this.placeholder = change.next();
        });
    }

    ngOnInit() {
        this.titleService.setTitle('ovaldo');
        let data: any = this.route.snapshot.data;
        this.languageProperty = data.language.value;
        this.closables.push(
            this.languageProperty.addListenerAndCall(
                this.onLanguageChange.bind(this)
            )
        );

        this.subscriptions.push(
            this.translateService.stream('home.meta.title')
                .subscribe((text: string) => {
                    this.titleService.setTitle(text);
                })
        );

        this.typewriter.start();
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
        this.typewriter.stop();
    }

    public onInputChange($event): void {
        this.showParselinkTooltip = this.isHostLink($event);
        this.connectRoomName = this.parseHostLink($event);
    }

    onSubmit(): void {
        if (!this.connectRoomName) {
            return;
        }
        
        this.router.navigate([this.connectRoomName]);
    }

    private isHostLink(text: string): boolean {
        return text.startsWith(environment.host + "/")
    }

    private parseHostLink(text: string): string {
        if (!this.isHostLink(text)) {
            return text;
        }
        try {
            return decodeURI(text.substr(environment.host.length + 1));
        } catch(exception) {
            return text.substr(environment.host.length + 1);
        }
    }

    public onLanguageChange(change: PropertyChange<string>): void {
        this.language = change.next();
    }

    public onLanguageSelect(language: string): void {
        this.dispatcher.dispatch(
            new Event("language.set", language)
        );
    }
}