import { Injectable } from "@angular/core";
import { GuestEntity } from "../entity/guest.entity";
import { ObservableProperty, SimpleObservableProperty } from '@wildebeest/observe-changes';

@Injectable()
export class GuestStore {
    private authUserValue: ObservableProperty<GuestEntity>;

    constructor() {
        this.authUserValue = new SimpleObservableProperty();
    }

    public authUser(): ObservableProperty<GuestEntity> {
        return this.authUserValue;
    }

    public authenticateUser(user: GuestEntity): void {
        this.authUserValue.set(user);
    }

    public signOut(): void {
        this.authUserValue.set(null);
    }
}