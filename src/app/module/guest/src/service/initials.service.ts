import { Injectable } from '@angular/core';

@Injectable()
export class InitialsService {
    private colors: Array<string> = [
        'strawberry',
        'orange',
        'tangerine',
        'lemon',
        'green-apple',
        'melon',
        'smurf',
        'blueberry',
        'blackberry',
        'grape'
    ];
    private users: any = {};

    constructor() { }

    public getColor(userId: number): string {
        if (!this.users[userId]) {
            let index: number = Math.floor(Math.random() * this.colors.length);
            if (index >= this.colors.length) {
                index = this.colors.length - 1;
            }
            this.users[userId] = this.colors[index];
        }
        return this.users[userId];
    }
}