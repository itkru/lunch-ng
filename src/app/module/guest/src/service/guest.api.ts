import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestFactory, RequestSender, RequestEntity } from "src/app/module/request";

@Injectable()
export class GuestApi {
    constructor(
        private requestFactory: RequestFactory,
        private requestSender: RequestSender
    ) {}

    public save(data: any): Observable<any> {
        let request: RequestEntity = this.requestFactory.createBuilder()
            .withPath('/users')
            .withMethod("POST")
            .withData(data)
            .build();

        return this.requestSender.send(request);
    }

    public autoLogin(): Observable<any> {
        let request: RequestEntity = this.requestFactory.createBuilder()
            .withPath('/status')
            .withMethod("GET")
            .build();

        return this.requestSender.send(request);
    }
}