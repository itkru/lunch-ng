import { NgModule } from '@angular/core';
import { GuestApi } from './service/guest.api';
import { RequestModule } from '../../request';
import { GuestStore } from './service/guest.store';
import { GuestFactory } from './entity/guest.factory';

@NgModule({
    imports: [RequestModule],
    exports: [],
    declarations: [],
    providers: [GuestApi, GuestStore, GuestFactory],
})
export class GuestModule {
    constructor () {}
}
