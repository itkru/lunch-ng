import { GuestEntity } from "./guest.entity";
import { Injectable } from "@angular/core";

@Injectable()
export class GuestFactory {
    private userColorMap: Map<string, string>;
    private colors: string[] = [
        "strawberry",
        "orange",
        "tangerine",
        "lemon",
        "green-apple",
        "melon",
        "smurf",
        "blueberry",
        "blackberry",
        "grape"
    ];

    constructor() {
        this.userColorMap = new Map();
    }

    public create(id: string, name: string): GuestEntity {
        if (!this.userColorMap.has(id)) {
            let randColor: string = this.getRandomColor();
            this.userColorMap.set(id, randColor);
        }
        let color: string = this.userColorMap.get(id);
        return new GuestEntity(id, name, color);
    }

    public createByObject(data: any): GuestEntity {
        return this.create(data.id, data.name);
    }

    private getRandomColor(): string {
        return this.colors[Math.min(this.colors.length - 1, Math.floor(Math.random() * this.colors.length))];
    }
}