export class GuestEntity {
    private initials;

    constructor(
        protected id: string,
        protected name: string,
        protected color: string
    ) {
        this.initials = this.createInitials();
    }

    public getId(): string {
        return this.id;
    }

    public getName(): string {
        return this.name;
    }

    public getInitials(): string {
        return this.initials;
    }

    public getColor(): string {
        return this.color;
    }

    private createInitials(): string {
        let name: string = this.getName();
        let splitName: Array<string> = name.split(" ");
        let initials: string = "";
        for (let part of splitName) {
            initials += part.charAt(0);
        }
        if (initials.length > 2) {
            initials = initials.substr(0, 2);
        }
        return initials;
    }

    public equals(guest: GuestEntity): boolean {
        return this.getId() === guest.getId()
            && this.getName() === guest.getName();
    }
}