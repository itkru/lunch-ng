export { RoomModule } from './src/room.module';

export { RoomStore } from './src/service/room.store';
export { RoomApi } from './src/service/room.api';
export { RoomEntity } from './src/entity/room.entity';
export { RoomFactory } from './src/entity/room.factory';