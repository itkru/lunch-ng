import { NgModule } from '@angular/core';
import { RoomApi } from './service/room.api';
import { RoomStore } from './service/room.store';
import { RoomFactory } from './entity/room.factory';

@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [RoomApi, RoomStore, RoomFactory],
})
export class RoomModule {
    
}
