export class RoomEntity {
    constructor(
        protected id: string,
        protected name: string
    ) {
        this.name = name;
    }

    public getId(): string {
        return this.id;
    }

    public getName(): string {
        return this.name;
    }

    public equals(room: RoomEntity): boolean {
        if (room === null || room === undefined) {
            return false;
        }
        return this.getId() === room.getId()
            && this.getName() === room.getName();
    }
}