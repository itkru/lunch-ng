import { RoomEntity } from "./room.entity";
import { Injectable } from "@angular/core";

@Injectable()
export class RoomFactory {
    public create(id: string, name: string): RoomEntity {
        return new RoomEntity(id, name);
    }
}