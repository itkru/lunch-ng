import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestSender, RequestFactory, RequestEntity } from "src/app/module/request";

@Injectable()
export class RoomApi {
    constructor (
        private requestSender: RequestSender,
        private requestFactory: RequestFactory
    ) {}

    public create(roomName: string): Observable<any> {
        let request: RequestEntity = this.requestFactory.createBuilder()
            .withMethod('POST')
            .withPath('/rooms')
            .withHeader("X-Timezone-Offset", (new Date()).getTimezoneOffset())
            .withData({name: roomName})
            .build();

        return this.requestSender.send(request);
    }

    public load(roomId: string): Observable<any> {
        let request: RequestEntity = this.requestFactory.createBuilder()
            .withMethod('GET')
            .withPath('/rooms/' + roomId)
            .withHeader("X-Timezone-Offset", (new Date()).getTimezoneOffset())
            .build();

        return this.requestSender.send(request);
    }

    public loadFilter(roomId: string, date: string): Observable<any> {
        let request: RequestEntity = this.requestFactory.createBuilder()
            .withMethod('GET')
            .withPath('/rooms/' + roomId)
            .withQuery('date', date)
            .withHeader("X-Timezone-Offset", (new Date()).getTimezoneOffset())
            .build();

        return this.requestSender.send(request);
    }

}