import { Injectable } from "@angular/core";
import { RoomEntity } from "../entity/room.entity";
import { ObservableProperty, SimpleObservableProperty } from '@wildebeest/observe-changes';

@Injectable()
export class RoomStore {
    private activeValue: ObservableProperty<RoomEntity>;

    constructor() {
        this.activeValue = new SimpleObservableProperty();
    }

    public active(): ObservableProperty<RoomEntity> {
        return this.activeValue;
    }

    public getActive(): ObservableProperty<RoomEntity> {
        return this.activeValue;
    }

    public activate(room: RoomEntity): void {
        if (!this.activeValue.isEmpty() && this.activeValue.get().equals(room)) {
            return;
        }
        this.activeValue.set(room);
    }

    public deactivate(): void {
        this.activate(null);
    }
}