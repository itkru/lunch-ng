import { NgModule } from "@angular/core";
import { ProxyLogger } from "./proxy-logger";

@NgModule({
    imports: [],
    declarations: [],
    exports: [],
    providers: [ProxyLogger]
})
export class LoggerModule {}