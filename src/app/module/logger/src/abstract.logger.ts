import { Logger } from "./logger";

export class AbstractLogger implements Logger {
    log(logType: string, message: string, context: any): void {}

    debug(message: string, context: any): void {
        this.log("debug", message, context);
    }

    info(message: string, context: any): void {
        this.log("info", message, context);
    }

    notice(message: string, context: any): void {
        this.log("notice", message, context);
    }

    warning(message: string, context: any): void {
        this.log("warning", message, context);
    }

    error(message: string, context: any): void {
        this.log("error", message, context);
    }

    alert(message: string, context: any): void {
        this.log("alert", message, context);
    }

    critical(message: string, context: any): void {
        this.log("critical", message, context);
    }

    emergency(message: string, context: any): void {
        this.log("emergency", message, context);
    }
}