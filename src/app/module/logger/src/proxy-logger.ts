import { Logger } from "./logger";
import { ErrorHandler, Injectable } from "@angular/core";
import { ConsoleLogger } from "./console-logger";

@Injectable()
export class ProxyLogger implements Logger, ErrorHandler {
    private proxy: Logger;

    constructor() {
        this.proxy = new ConsoleLogger();
    }

    public setProxy(proxy: Logger): void {
        this.proxy = proxy;
    }

    handleError(ex) {
        this.alert(ex.message, {exception: ex});
        throw ex;
    }

    debug(message: string, context: any): void {
        this.proxy.debug(message, context);
    }

    info(message: string, context: any): void {
        this.proxy.info(message, context);
    }

    notice(message: string, context: any): void {
        this.proxy.notice(message, context);
    }

    warning(message: string, context: any): void {
        this.proxy.warning(message, context);
    }

    error(message: string, context: any): void {
        this.proxy.error(message, context);
    }

    alert(message: string, context: any): void {
        this.proxy.alert(message, context);
    }

    critical(message: string, context: any): void {
        this.proxy.critical(message, context);
    }

    emergency(message: string, context: any): void {
        this.proxy.emergency(message, context);
    }
}
