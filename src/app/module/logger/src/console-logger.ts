import { Logger } from "./logger";
import { Injectable } from "@angular/core";

export class ConsoleLogger implements Logger {
    debug(message: string, context: any): void {
        console.debug(message, context);
    }

    info(message: string, context: any): void {
        console.info(message, context);
    }

    notice(message: string, context: any): void {
        console.info(message, context);
    }

    warning(message: string, context: any): void {
        console.warn(message);
    }

    error(message: string, context: any): void {
        console.error(message, context);
    }

    alert(message: string, context: any): void {
        console.error(message, context);
    }

    critical(message: string, context: any): void {
        console.error(message, context);
    }

    emergency(message: string, context: any): void {
        console.error(message, context);
    }

    
}