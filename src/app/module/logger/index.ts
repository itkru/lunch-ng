export { LoggerModule } from './src/logger.module';
export { Logger } from './src/logger';
export { AbstractLogger } from './src/abstract.logger';
export { ProxyLogger } from './src/proxy-logger';