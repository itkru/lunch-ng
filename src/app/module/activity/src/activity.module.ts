import { NgModule } from '@angular/core';
import { ActivityStore } from './service/activity.store';
import { ActivityApi } from './service/activity.api';
import { ActivityFactory } from './entity/activity.factory';

@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [ActivityApi, ActivityStore, ActivityFactory],
})
export class ActivityModule {
    
}
