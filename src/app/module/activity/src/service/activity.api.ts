import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { RequestSender, RequestFactory, RequestEntity } from "src/app/module/request";
import { ActivityEntity } from "../entity/activity.entity";

@Injectable()
export class ActivityApi {
    constructor (
        private requestSender: RequestSender,
        private requestFactory: RequestFactory,
        private router: Router
    ) {}

    public create(activity: ActivityEntity): Observable<any> {
        let placeObject: any = {
            id: activity.getPlace().getId(),
            external_id: activity.getPlace().getExternalId(),
            name: activity.getPlace().getName()
        };

        let request: RequestEntity = this.requestFactory.createBuilder()
            .withMethod('POST')
            .withPath("/rooms/" + activity.getRoomId() + "/activities")
            .withHeader("X-Timezone-Offset", (new Date()).getTimezoneOffset())
            .withData({
                place: placeObject,
                starts_at: activity.getStartAt(),
                text: activity.getInfo()
            })
            .build();

        return this.requestSender.send(request);
    }

    public join(activity: ActivityEntity): Observable<any> {
        let request: RequestEntity = this.requestFactory.createBuilder()
            .withMethod('POST')
            .withPath('/activities/' + activity.getId() + '/users')
            .withHeader("X-Timezone-Offset", (new Date()).getTimezoneOffset())
            .withData({
                action: 'join'
            })
            .build();
        
        return this.requestSender.send(request);
    }

    public leave(activity: ActivityEntity): Observable<any> {
        let request: RequestEntity = this.requestFactory.createBuilder()
            .withMethod('POST')
            .withPath('/activities/' + activity.getId() + '/users')
            .withHeader("X-Timezone-Offset", (new Date()).getTimezoneOffset())
            .withData({
                action: 'leave'
            })
            .build()

        return this.requestSender.send(request);
    }
}