import { Injectable } from "@angular/core";
import { ActivityEntity } from "../entity/activity.entity";
import { ObservableMap, SimpleObservableMap, ObservableProperty, SimpleObservableProperty } from '@wildebeest/observe-changes';

@Injectable()
export class ActivityStore {
    private activitiesValue: ObservableMap<string, ActivityEntity>;
    private selectedValue: ObservableProperty<ActivityEntity>;

    constructor() {
        this.activitiesValue = new SimpleObservableMap();
        this.selectedValue = new SimpleObservableProperty();
    }

    public activities(): ObservableMap<string, ActivityEntity> {
        return this.activitiesValue;
    }

    public selected(): ObservableProperty<ActivityEntity> {
        return this.selectedValue;
    }

    public select(activity: ActivityEntity): void {
        this.selected().set(activity);
    }

    public deselect(): void {
        if (this.selectedValue.isEmpty()) {
            return;
        }
        this.select(null);
    }
}