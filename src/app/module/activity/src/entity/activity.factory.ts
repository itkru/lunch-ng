import { PlaceEntity, PlaceFactory } from "src/app/module/place";
import { ActivityEntity } from "./activity.entity";
import { Injectable } from "@angular/core";
import { GuestEntity, GuestFactory } from "src/app/module/guest";

@Injectable()
export class ActivityFactory {
    constructor(
        private placeFactory: PlaceFactory,
        private guestFactory: GuestFactory
    ) {}

    public create(id: string, roomId: string, startAt: Date, place: PlaceEntity, info: string, users: GuestEntity[]): ActivityEntity {
        return new ActivityEntity(id, roomId, startAt, place, info, users);
    }

    public createByObject(data: any): ActivityEntity {
        let users: GuestEntity[] = [];
        for (let user of data.users) {
            users.push(
                this.guestFactory.createByObject(user)
            );
        }
        return this.create(
            data.id,
            "",
            new Date(data.starts_at),
            this.placeFactory.createByObject(data.place),
            data.text,
            users
        );
    }
}