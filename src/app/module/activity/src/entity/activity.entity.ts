import { PlaceEntity } from "src/app/module/place";
import { GuestEntity } from "src/app/module/guest";

export class ActivityEntity {
    constructor(
        protected id: string,
        protected roomId: string,
        public startAt: Date,
        public place: PlaceEntity,
        public info: string,
        protected users: GuestEntity[]
    ) {}

    public getId(): string {
        return this.id;
    }

    public getRoomId(): string {
        return this.roomId;
    }

    public setRoomId(roomId: string): void {
        this.roomId = roomId;
    }

    public getStartAt(): Date {
        return this.startAt;
    }

    public getPlace(): PlaceEntity {
        return this.place;
    }

    public getUsers(): GuestEntity[] {
        return this.users;
    }

    public getInfo(): string {
        return this.info;
    }

    public hasUser(userId: string): boolean {
        for (let user of this.getUsers()) {
            if (user.getId() === userId) {
                return true;
            }
        }
        return false;
    }

    public equals(activity: ActivityEntity): boolean {
        let result: boolean = this.getId() === activity.getId()
            && this.getInfo() === activity.getInfo()
            && this.getStartAt().getTime() === activity.getStartAt().getTime()
            && this.getPlace().equals(activity.getPlace())
            && this.getUsers().length === activity.getUsers().length;
        if (!result) {
            return false;
        }

        let selfUsers: GuestEntity[] = this.getUsers();
        let activityUsers: GuestEntity[] = activity.getUsers();
        for (let i = 0; i < selfUsers.length; i++) {
            if (!selfUsers[i].equals(activityUsers[i])) {
                return false;
            }
        }
        return true;
    }
}