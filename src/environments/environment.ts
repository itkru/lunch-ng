export const environment = {
    production: false,
    host: 'http://localhost:4200',
    analytics: {
        use: 'none',
        gtag: null
    },
    log: {
        use: 'none',
        sentry: null
    },
    server: {
        host: 'http://php.ovaldo.local'
    }
};
