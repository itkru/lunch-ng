# Ovaldo [angular]

## Requirements

1. node >10.9
2. [yarn](https://yarnpkg.com/lang/en/docs/install/#debian-stable)
3. [angular cli](https://cli.angular.io/)

## Instalation 

1. clone the repository
2. run `yarn install` in project folder
3. run `ng serve` - angular cli command
4. you should be able to se [web](http://localhost:4200/) and any change you make will be automaticaly transferd to browser (you dont have to refresh the page or rebuild scripts)

## IDE (recommended)

1. install [VScode](https://code.visualstudio.com/)
2. install plugins:
 
  * angular language service
  * angular v6 snippets
  * gitlens
  * git history

## Modules

### Main module

Containes all business logic and listens to gui event

### Gui Module

Contains all gui components views and directives. It will not have any logic inside. Use Events dispatcher to notify application that something happend.

### Domain modules

Each domain can have separate module, that contain entities (models) and stores (repositories)